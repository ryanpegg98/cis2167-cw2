<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scale extends Model
{
    protected $fillable = [
        'question_id',
        'start',
        'end',
        'positions',
    ];

    /**
     * Get the question that it belongs to
     */

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

}
