<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Gate;
use Auth;
use App\Http\Requests;
use App\Questionnaire;
use App\Question;
use App\Option;

class OptionsController extends Controller
{
    /**
     *
     * Secure the users pages to just allow admins to see them
     *
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($questionnaire, $question)
    {
        // Redirect them to the questions page
        return redirect('/questionnaires/' . $questionnaire . '/questions/' . $question);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($questionnaire, $question)
    {
        // Redirect them to the questions page
        return redirect('/questionnaires/' . $questionnaire . '/questions/' . $question);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($questionnaire, $question, Request $request)
    {


        // Check that they are allowed to edit the questionnarie
        $questionnaire_count = Questionnaire::where('slug', $questionnaire)->count();
        if($questionnaire_count == 1){
            // check they can edit the questionnaire and if they own the questionnaire
            $questionnaire = Questionnaire::where('slug', $questionnaire)->first();
            $question_count = Question::where([
                ['questionnaire_id' , '=', $questionnaire->id],
                ['slug', '=', $question]
            ])->count();
            if(Gate::allows('edit_questionnaires') && $questionnaire->creator_id == Auth::user()->id){
                //check the question exists
                if($question_count == 1){
                    // if the question exists show them the form
                    $question = Question::where([
                        ['questionnaire_id' , '=', $questionnaire->id],
                        ['slug', '=', $question]
                    ])->first();

                    //validate that the form and the questionnaire and question exists
                    $this->validate($request, [
                        'questionnaire' => 'required|alpha_dash',
                        'question' => 'required|alpha_dash',
                        'option' => 'required|min:1|max:255|unique:options,option,NULL,id,question_id,' . $question->id,
                    ]);

                    $input = [
                        'question_id' => $question->id,
                        'option' => $request->get('option')
                    ];

                    $option = Option::create($input);

                    return redirect('/questionnaires/' . $questionnaire->slug . '/questions/' . $question->slug)->withSuccess('"' . $option->option . '" has been added.');

                } else {
                    return redirect('/questionnaires/' . $questionnaire->slug)->withError('That question does not exist.');
                }
            } else {
                // if the user is not allowed to edit the questionnaire return them to the questionnaires page
                return redirect('/questionnaires/' . $questionnaire->slug)->withError('You do not have permission to edit this questionnaire.');
            }
        } else {
            // if it doesn't exist return them to the questionnaires page
            return redirect('/questionnaires')->withError('That questionnaire does not exist.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
