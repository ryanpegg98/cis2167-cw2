<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Questionnaire;
use App\Question;
use App\Option;
use App\Scale;
use App\Respondent;
use App\Response;
use App\User;

class ResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get all of the questionnaires that are open to the respondents
        $questionnaires = Questionnaire::where('status', 1)->get();

        return view('responses.index')->with('questionnaires', $questionnaires);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // check the questionnaire exists
        $form = $request->all();

        $questionnaire = Questionnaire::where('slug', $form['questionnaire'])->first();
        if(count($questionnaire) > 0){
            //check the layout is correct
            if($questionnaire->layout == 1 && $form['layout'] == 'listed'){
                //create a respondent
                $respondent = Respondent::create(['questionnaire_id' => $questionnaire->id]);
                //return 'lsited';
                $fieldlist = array_keys($form);
                //loop through all the fields
                for($i = 0; $i < count($fieldlist); $i++){
                    $question = $fieldlist[$i];
                    if($question != '_token' && $question != 'questionnaire' && $question != 'layout') {
                        // create a response for each question` linking it to the reponse

                        $response = $form[$question];
                        $storage = [
                            'respondent_id' => $respondent->id,
                            'question_id' => $question,
                            'response' => $response
                        ];

                        // time to store the response
                        Response::create($storage);
                    }
                }

                // redirect the user to the home page with success message
                return redirect('/respond')->withSuccess('Your response for "' . $questionnaire->title . '" has been recorded.');
            } elseif($questionnaire->layout == 1 && $form['layout'] == 'one'){

            } else {
                return redirect('/home')->withError('That questionnaire does not exist. Please select a different one.');
            }
        } else {
            return redirect('/home')->withError('That questionnaire does not exist. Please select a different one.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // check the questionnaire exists and is open
        $questionnaire = Questionnaire::where('slug', $id)->first();
        if(count($questionnaire) == 1){
            // check that the questionanire is open
            if($questionnaire->status == 1){
                //get the user name
                $user = User::where('id', $questionnaire->creator_id)->first();
                $questionnaire['name'] = $user->name;
                    //User::where('id', $questionnaire->creator_id)->first();
                // send them the information that they requested
                return view('responses.show')->with('questionnaire', $questionnaire);
            } else {
                // tell them to select a new questionnaire if it can not be responded to
                return redirect('/home')->withError('That questionnaire does not exist. Please select a different one.');
            }
        } else {
            // send them to pick a different questionnaire
            return redirect('/home')->withError('That questionnaire does not exist. Please select a different one.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // check the questionnaire exists
        return redirect('/home')->withError('That questionnaire does not exist. Please select a different one.');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return redirect('/home')->withError('That questionnaire does not exist. Please select a different one.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return redirect('/home')->withError('That questionnaire does not exist. Please select a different one.');
    }

    public function start_ques($questionnaire)
    {
        // check the questionnaire exists
        $questionnaire = Questionnaire::where('slug', $questionnaire)->first();
        if(count($questionnaire) > 0){
            //get the user
            $user = User::where('id', $questionnaire->creator_id)->first();
            $questionnaire['name'] = $user->name;
            // check the questionnaire is open
            if($questionnaire->status == 1){
                // get all the questions
                $questions = Question::where('questionnaire_id', $questionnaire->id)->get();

                foreach($questions as $question){
                    $storeoptions = array();
                    if($question->type == 3){
                        // get all options
                        $options = Option::where('question_id', $question->id)->get();
                        foreach($options as $option){
                            $storeoptions[$option->id] = $option->option;
                        }
                    } elseif ($question->type == 4){
                        // Fetch the scale and seperate between the different positions
                        $scale = Scale::where('question_id', $question->id)->first();
                        for($i = 0; $i <= $scale->positions; $i++){
                            if($i == 0){
                                $storeoptions[$i] = $scale->start;
                            } elseif ($i == $scale->positions){
                                $storeoptions[$i] = $scale->end;
                            } else {
                                $storeoptions[$i] = '.';
                            }
                        }

                    }
                    $question['options'] = $storeoptions;
                }

                //see which type of questionnaire it is
                if($questionnaire->layout == 1){
                    // the questionnaire is listed
                    return view('responses.listed')->with('questionnaire', $questionnaire)->with('questions', $questions);

                } elseif ($questionnaire->layout == 2){

                } else {
                    //return the error message
                    return redirect('/home')->withError('That questionnaire does not exist. Please select a different one.');
                }
            } else {
                return redirect('/home')->withError('That questionnaire does not exist. Please select a different one.');
            }
        } else {
            // Send the user back to the questionnaires page
            return redirect('/home')->withError('That questionnaire does not exist. Please select a different one.');
        }

    }

//    public function start_ques($questionnaire)
//    {
//        // check the questionnaire exists
//        $questionnaire = Questionnaire::where('slug', $questionnaire)->first();
//        if(count($questionnaire) > 0){
//            // check the questionnaire is open
//            if($questionnaire->status == 1){
//
//            } else {
//                return redirect('/home')->withError('That questionnaire does not exist. Please select a different one.');
//            }
//        } else {
//            // Send the user back to the questionnaires page
//            return redirect('/home')->withError('That questionnaire does not exist. Please select a different one.');
//        }
//
//    }
}
