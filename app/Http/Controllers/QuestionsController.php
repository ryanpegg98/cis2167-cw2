<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Gate;
use Auth;
use App\Http\Requests;
use App\Questionnaire;
use App\Question;
use App\Option;
use App\Scale;

class QuestionsController extends Controller
{

    /**
     *
     * Secure the users pages to just allow admins to see them
     *
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($questionnaire)
    {
        //redirect to the questionnaire page
        return redirect('/questionnaires/' . $questionnaire);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($questionnaire)
    {
        //check that the questionnaire exists
        $questionnaires = Questionnaire::where('slug', $questionnaire)->count();

        if($questionnaires == 1){
            // Check to see if they are allowed to edit questionnaires
            if(Gate::allows('edit_questionnaires')){
                // get the questionnaire details
                $data = Questionnaire::where('slug', $questionnaire)->first();

                //return var_dump($data->title);
                //send the user to the correct view
                return view('questionnaires.questions.create')->with('questionnaire', $data);
            } else {
                // Re-direct the user back if they cannot
                return redirect('/questionnaires/' . $questionnaire)->withError('You do not have the correct permissions to complete that action.');
            }
        } else {
            return redirect('/questionnaires')->withError('The questionnaire does not exist.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $questionnaire)
    {
        //Check that the user is allowed to create a question
        if(Gate::allows('edit_questionnaires')){
            //Check that the questionnaire exists
            $count = Questionnaire::where('slug', $questionnaire)->count();
            //check that the count is equal to 1
            if($count == 1){
                $questionnaire = Questionnaire::where('slug', $questionnaire)->first();
                //Store the question
                $this->validate($request, [
                    'question' => 'required|min:3|max:255|unique:questions,question,NULL,id,questionnaire_id,' . $questionnaire->id,
                    'slug' => 'required|alpha_dash|min:1|max:255|unique:questions,slug,NULL,id,questionnaire_id,' . $questionnaire->id,
                    'type' => 'required|integer|min:1|max:4',
                ]);

                //Create the question
                $input = $request->all();
                $input['questionnaire_id'] = $questionnaire->id;

                if($input['type'] > 2 && !isset($input['layout'])){
                    return view('questionnaires.questions.createlayout')->with('question', $input)->with('questionnaire', $questionnaire);
                }

                $question = Question::create($input);

                if($input['type'] == 4){
                    //create the scale for them to be redirected to and changed
                    $scale_values = [
                      'question_id' => $question->id,
                      'start' => 'Start',
                      'end' => 'End',
                      'positions' => 3
                    ];

                    // create the scale then redirect the user to that scale page
                    Scale::create($scale_values);

                    return redirect('/questionnaires/' . $questionnaire->slug . '/questions/' . $question->slug . '/scales/edit');
                } elseif ($input['type'] == 3){
                    //send the user to the question page
                    return redirect('/questionnaires/' . $questionnaire->slug . '/questions/' . $question->slug);
                }


                return redirect('/questionnaires/' . $questionnaire->slug )->withSuccess('"' . $question->question . '" has been created.');

            } else {
                //Redirect the user to the questionnaires page
                return redirect('/questionnaires')->withError('The questionnaire does not exist.');
            }
        } else {
            //send the user back to the home page
            return redirect('/questionnaires/' . $questionnaire)->withError('You do not have the correct permissions to complete that action.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($questionnaire, $id)
    {
        //check that the user can edit the question before they can go on to edit the questionnaire
        if(Gate::allows('edit_questionnaires')){
            // grab the questionnaires information as well as the question
            $questionnaire_count = Questionnaire::where('slug', $questionnaire)->count();

            if($questionnaire_count == 1){
                $questionnaire = Questionnaire::where('slug', $questionnaire)->first();
                // check that the questionnaire is the users
                if($questionnaire->creator_id == Auth::user()->id){
                    //get the questions and send them to the show view
                    $question = Question::where([
                        ['questionnaire_id', '=', $questionnaire->id],
                        ['slug', '=', $id]
                    ])->count();

                    if($question == 1) {
                        $question = Question::where([
                            ['questionnaire_id', '=', $questionnaire->id],
                            ['slug', '=', $id]
                        ])->first();
                        // if the question is an options question
                        if ($question->type == 3) {
                            $options = Option::where('question_id', $question->id)->get();
                            $question['options'] = $options;
                        }
                        //send them to the view
                        return view('questionnaires.questions.show')->with('questionnaire', $questionnaire)->with('question', $question);
                    } else {
                        return redirect('/questionnaires/' . $questionnaire->slug)->withError('That question does not exist.');
                    }
                } else {
                    // redirect them to their questionnaires page
                    return redirect('/questionnaires/')->withError('You do not have the correct permissions to access this information.');
                }
            } else {
                return redirect('/questionnaires')->withError('The questionnaire does not exist.');
            }


        } else {
            return redirect('/questionnaires/' . $questionnaire)->withError('You do not have the correct permissions to access this information.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
