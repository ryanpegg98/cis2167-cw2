<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Gate;
use Auth;
use App\Http\Requests;
use App\Questionnaire;
use App\Question;
use App\Option;
use App\Scale;
use App\Respondent;
use App\Response;

class QuestionnairesController extends Controller
{

    /**
     *
     * Secure the users pages to just allow admins to see them
     *
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get all of the user's questionnaires
        $questionnaires = Questionnaire::where('creator_id', Auth::user()->id)->orderBy('updated_at', 'desc')->get();

        return view('questionnaires.index')->with('questionnaires', $questionnaires);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // show the user the create form if they are allowed
        if(Gate::allows('create_questionnaires')){
            return view('questionnaires.create');
        } else {
            // if they dont have the correct permissions then redirect them
            return redirect('/questionnaires')->withError('You do not have the correct permissions to complete that action.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // check the user is allowed to create questionnaires
        if(Gate::allows('create_questionnaires')){
            $this->validate($request, [
                'title' => 'required|unique:questionnaires|min:4|max:255',
                'slug' => 'required|unique:questionnaires|min:2|max:255|alpha_dash',
                'description' => 'required|min:5',
                'layout' => 'required|integer'
            ]);

            $input = $request->all();
            $input['creator_id'] = Auth::user()->id;
            $input['status'] = 0;

            $questionnaire = Questionnaire::create($input);

            return redirect('/questionnaires')->withSuccess('"' . $questionnaire->title . '" has been created.');
        } else {
            // If the user cant create questions tell them
            return redirect('/questionnaires')->withError('You do not have the correct permissions to complete that action.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Get the questionnaire
        $questionnaire = Questionnaire::where('slug', $id)->first();

        //check for the questionnaire is the user
        if(Auth::user()->id == $questionnaire->creator_id && $questionnaire->status == 0){
            // get 5 the questions that belong to the questionnaire
            $questions = Question::where('questionnaire_id', $questionnaire->id)->get();
            // send the information to the show view
            return view('questionnaires.show')->with('questionnaire', $questionnaire)->with('questions', $questions);
        } else {
            return redirect('/questionnaires')->withError('You do not have the correct permissions to access that information.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Check that the user can edit the questionnaires
        if(Gate::allows('edit_questionnaires')){
            $questionnaire = Questionnaire::where('slug', $id)->first();
            //check to see if the questionnair belongs to the user
            if(Auth::user()->id == $questionnaire->creator_id && $questionnaire->status == 0){
                //send the questionnaire data to the edit view
                return view('questionnaires.edit')->with('questionnaire', $questionnaire);
            } else {
                //return them to their questionnaires
                return redirect('/questionnaires')->withError('You do not have the correct permissions access that information.');
            }
        } else {
            // If they cant return them to the home page
            return redirect('/questionnaires')->withError('You do not have the correct permissions to complete that action.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Check that the user is able to edit the questionnaire
        if(Gate::allows('edit_questionnaires')){
            $questionnaire = Questionnaire::where('slug', $id)->first();
            //check to see if the questionnair belongs to the user
            if(Auth::user()->id == $questionnaire->creator_id && $questionnaire->status == 0){
                //now validate the request
                $this->validate($request, [
                    'title' => 'required|min:4|max:255|unique:questionnaires,title,' . $questionnaire->id,
                    'slug' => 'required|min:2|max:255|alpha_dash|unique:questionnaires,slug,' . $questionnaire->id,
                    'description' => 'required|min:5',
                    'layout' => 'required|integer'
                ]);

                //now change the values in the database
                $input = $request->all();
                $questionnaire->update($input);

                //send the questionnaire data to the edit view
                return redirect('/questionnaires')->withSuccess('"' . $questionnaire->title . '" has been updated.');
            } else {
                //return them to their questionnaires
                return redirect('/questionnaires')->withError('You do not have the correct permissions access that information.');
            }
        } else {
            // If they cant return them to the home page
            return redirect('/questionnaires')->withError('You do not have the correct permissions to complete that action.');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // check that the user can delete the questionnaire
        if(Gate::allows('delete_questionnaires')){
            //get the questionnaire to make sure it can be found
            $questionnaire = Questionnaire::where('slug', $id)->first();
            //check that the user owns this questionnaire
            if(Auth::user()->id == $questionnaire->creator_id){
                //get the title of the questionnaire
                $title = $questionnaire->title;
                // delete the questionnaire
                $questionnaire->delete();
                //returnt the user to the questionnaires page
                return redirect('/questionnaires')->withSuccess('"' . $title . '" has been deleted.');
            } else {
                // If they are not return them to the home page
                return redirect('/questionnaires')->withError('You do not have the correct permissions to complete that action.');
            }
        } else {
            // If they cant return them to the home page
            return redirect('/questionnaires')->withError('You do not have the correct permissions to complete that action.');
        }
    }

    /**
     * This will take a request with the questionnaire slug and check that the
     * user owns it the
     */

    public function open_questionnaire(Request $request)
    {
        // Check that the user is allowed
        if(Gate::allows('edit_questionnaires')){
            // get the questionanire to see if it exists
            $questionnaire = Questionnaire::where('slug', $request->get('questionnaire'))->first();
            if(count($questionnaire) == 1) {
                // Check that the user owns the questionanire
                if(Auth::user()->id == $questionnaire->creator_id && $questionnaire->status == 0){
                    // if the user does own it then complete the functiona and set the row to the column
                    $query = [
                        'status' => 1
                    ];

                    // update the questionnaire
                    $questionnaire->update($query);

                    //return the user to the questionnaire page
                    return redirect('/questionnaires')->withSuccess('"' . $questionnaire->title . '" has been opened to the public.');
                } else {
                    // if the user doesnt own the questionanire return them to the correct page
                    return redirect('/questionnaires')->withError('You do not have the correct permissions to complete that action');
                }

            } else {
                return redirect('/questionnaires')->withError('That questionnaire doe not exist');
            }
        } else {
            // Redirect them to the questionnaires page
            return redirect('/questionnaires')->withError('You do not have the correct permissions to complete that action');
        }
    }

    /**
     * The user will be checked and validated prior to the action being completed
     * This will make the questionnaire no longer visible to the public
     */

    public function close_questionnaire(Request $request)
    {
        // Check that the user is allowed
        if(Gate::allows('edit_questionnaires')){
            // get the questionanire to see if it exists
            $questionnaire = Questionnaire::where('slug', $request->get('questionnaire'))->first();
            if(count($questionnaire) == 1) {
                // Check that the user owns the questionanire
                if(Auth::user()->id == $questionnaire->creator_id){
                    // if the user does own it then complete the functiona and set the row to the column
                    $query = [
                        'status' => 2
                    ];

                    // update the questionnaire
                    $questionnaire->update($query);

                    //return the user to the questionnaire page
                    return redirect('/questionnaires')->withSuccess('"' . $questionnaire->title . '" is no longer open to the public.');
                } else {
                    // if the user doesnt own the questionanire return them to the correct page
                    return redirect('/questionnaires')->withError('You do not have the correct permissions to complete that action');
                }

            } else {
                return redirect('/questionnaires')->withError('That questionnaire doe not exist');
            }
        } else {
            // Redirect them to the questionnaires page
            return redirect('/questionnaires')->withError('You do not have the correct permissions to complete that action');
        }
    }

    /**
     * This will fetch all of the responses to the questionnaire and show them to the admin
     */

    public function responses($questionnaire)
    {
        // get the questionnaire and check that it is owned by the user
        $questionnaire = Questionnaire::where('slug', $questionnaire)->first();
        if(count($questionnaire) > 0 && $questionnaire->creator_id == Auth::user()->id){
            // check the status of the questionnaire
            if($questionnaire->status == 1 || $questionnaire->status == 2){
                // get all of the respondents
                $respondents = Respondent::where('questionnaire_id', $questionnaire->id)->get();
                foreach($respondents as $respondent){
                    // get their responses
                    $responses = Response::where('respondent_id', $respondent->id)->get();
                    $respondent['responses'] = $responses;

                }

                $questions = Question::where('questionnaire_id', $questionnaire->id)->get();
                //loop through the questions and give the options and scales
                foreach($questions as $question){
                    $storeoptions = array();// store the scale and the options in an array
                    if($question->type == 3){
                        //get the options
                        $options = Option::where('question_id', $question->id)->get();
                        $storeoptions = $options;
                    } elseif($question->type == 4){
                        //get the scale for the question
                        $scale = Scale::where('question_id', $question->id)->first();
                        $storeoptions = $scale;
                    }

                    $question['options'] = $storeoptions;
                }

                // send the users to the responses view with the questionanire, questions and responses
                return view('questionnaires.responses')->with('questionnaire', $questionnaire)->with('questions', $questions)->with('respondents', $respondents);
            } else {
                // if not send them back to the questionnaires page
                return redirect('/questionnaires')->withError('You do not have the correct permissions to complete that action');
            }
        } else {
            return redirect('/questionnaires')->withError('That questionnaire doe not exist');
        }
    }
}
