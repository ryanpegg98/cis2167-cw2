<?php

namespace App\Http\Controllers;

use Gate;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Role;
use App\Questionnaire;

class UsersController extends Controller
{
    /**
     *
     * Secure the users pages to just allow admins to see them
     *
     */

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::allows('see_users')) {
            // Get the users in alphabetical order
            // Dont get the current user as they should not be able to change their own roles and
            // should use a different operation
            $users = User::where('id', '!=', Auth::user()->id)->orderBy('name', 'asc')->get();
            return view('admin.users.index')->with('users', $users);
        } else {
            return redirect('/')->withError('You do not have the correct permissions to access that information');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // see if the user can create another user
        if(Gate::allows('create_users')){
            // Give the user the form to create a user
            return view('admin.users.create');
        } else {
            // Otherwise return them to the admin page
            return redirect('/admin/users')->withError('You do not have the correct permissions to complete that action.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // check that the user can create users
        if(Gate::allows('create_users')){
            //validate the request
            $this->validate($request, [
                'name' => 'required|min:3|max:255',
                'email' => 'required|unique:users|min:4|max:255|email',
                'password' => 'required|min:6|max:50'
            ]);

            // Time to create the user
            $input = $request->all();
            $password = $input['password'];
            $input['password'] = bcrypt($input['password']);

            //create the record in the database
            $user = User::create($input);

            // Give the user the default role of Creator
            $user->roles()->sync([2]);

            // return the user to the page with success message
            return redirect('/admin/users')->withSuccess('"' . $user->name . '" has been created.');
        } else {
            // redirect them to the admin page
            return redirect('/admin/users')->withError('You do not have the correct permissions to complete that action.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Only allow the users who can edit the users to see the form
        if(Gate::allows('see_users')){
            // Get the user or fail
            $user = User::findOrFail($id);

            // Get the users questionnaires
            $questionnaires = Questionnaire::where('creator_id', $id)->orderBy('updated_at', 'asc')->get();

            // show the user the form to edit the specified user
            return view('admin.users.show')->with('user', $user)->with('questionnaires', $questionnaires);
        } else {
            // Send the user back to the users page
            return redirect('/admin/users')->withError('You do not have the correct permissions to access that information');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Only allow the users who can edit the users to see the form
        if(Gate::allows('edit_users')){
            // Get the user or fail
            $user = User::findOrFail($id);

            //if the user can assign roles send roles with the user
            $roles = array();

            if(Gate::allows('assign_roles')){
                $roles = Role::all();
            }

            // show the user the form to edit the specified user
            return view('admin.users.edit')->with('user', $user)->with('roles', $roles);
        } else {
            // Send the user back to the users page
            return redirect('/admin/users')->withError('You do not have the correct permissions to complete that action.');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // see if the user can update the users details
        if(Gate::allows('edit_users')){
            // validate the new details the email needs to be unique but do not include
            //the current user in that uniqueness
            $this->validate($request, [
                'name' => 'required|min:3|max:255',
                'email' => 'required|min:4|max:255|email|unique:users,email,' . $id,
            ]);

            //update the details
            $input = $request->all();
            $user = User::findOrFail($id);
            $user->update($input);

            // check to see if the user has defined a role
            if(Gate::allows('assign_roles')){
                $roles = $request->get('role');
                if(count($roles) < 1){
                    $roles = [2];// Assign the the default of creator
                }

                $user->roles()->sync($roles);
            }

            return redirect('/admin/users')->withSuccess('"' . $user->name . '" has been updated.');

        } else {
            // Send the user back to the users page
            return redirect('/admin/users')->withError('You do not have the correct permissions to complete that action.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Check the user can delete users
        if(Gate::allows('delete_users')){
            //delete the user
            $user = User::findOrFail($id);
            $name = $user->name;
            $user->delete();

            // Send the user back to the users page
            return redirect('/admin/users')->withSuccess('"' . $name . '" has been deleted.');

        } else {
            // send the user back to the appropriate page with error message
            return redirect('/admin/users')->withError('You do not have the correct permissions to complete that action.');
        }
    }

    /**
     * Form to reset the users password on request
     */

    public function reset_form($id)
    {
        //Check the user is allowed to reset passwords

        if(Gate::allows('reset_passwords')) {
            $user = User::findOrFail($id);

            return view('admin.users.resetpassword')->with('user', $user);
        } else {
            return redirect('/admin/users')->withError('You do not have the correct permissions to complete that action.');
        }
    }

    /**
     * Update the users new password if the current user has the correct permissions
     */

    public function reset_password(Request $request, $id)
    {

        //Check the user is allowed to reset passwords

        if(Gate::allows('reset_passwords')) {

            $this->validate($request, [
                'new_password' => 'bail|required|min:6|max:50',
                'confirm_password' => 'required|min:6|max:50|same:new_password'
            ]);

            $user = User::findOrFail($id);

            $data['password'] = bcrypt($request->get('new_password'));

            $user->update($data);

            return redirect('/admin/users/')->withSuccess('"' . $user->name . '" has had their password reset.');
        } else {
            return redirect('/admin/users')->withError('You do not have the correct permissions to complete that action.');
        }
    }

}
