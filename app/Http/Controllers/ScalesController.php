<?php

namespace App\Http\Controllers;


use App\Scale;
use Illuminate\Http\Request;

use Gate;
use Auth;
use App\Http\Requests;
use App\Questionnaire;
use App\Question;

class ScalesController extends Controller
{

    /**
     *
     * Secure the users pages to just allow admins to see them
     *
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($questionnaire, $question)
    {
        //check that the user can edit the questionnaire and that the questionnaire and the question exists
        $questionnaire_count = Questionnaire::where([
            ['slug', '=', $questionnaire],
            ['creator_id', '=', Auth::user()->id]
        ])->count();
        if(Gate::allows('edit_questionnaires') && $questionnaire_count == 1){
            // Get the questionnaire and check that the question exists
            $questionnaire = Questionnaire::where([
                ['slug', '=', $questionnaire],
                ['creator_id', '=', Auth::user()->id]
            ])->first();
            $question_count = Question::where([
                ['slug', '=', $question],
                ['questionnaire_id', '=', $questionnaire->id]
            ])->count();
            // check that it exists
            if($question_count == 1){
                //if the question exists fetch it and the scale to send to the edit page
                $question = Question::where([
                    ['slug', '=', $question],
                    ['questionnaire_id', '=', $questionnaire->id]
                ])->first();
                // get the scale
                $scale = Scale::where('question_id', $question->id)->first();

                // send the user to the edit page for the scale
                return view('questionnaires.questions.scales.edit')->with('questionnaire', $questionnaire)->with('question', $question)->with('scale', $scale);

            } else {
                return redirect('/questionnaires/' . $questionnaire->slug)->withError('That question does not exist.');
            }
        } else {
            return redirect('/questionnaires/' . $questionnaire)->withError('You are unable to edit this questionnaire.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect('/questionnaires/' . $questionnaire . '/questions/' . $question . '/scales');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($questionnaire, $question, $id)
    {
        return redirect('/questionnaires/' . $questionnaire . '/questions/' . $question . '/scales');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($questionnaire, $question, $id)
    {
        return redirect('/questionnaires/' . $questionnaire . '/questions/' . $question . '/scales');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($questionnaire, $question, Request $request, $id)
    {
        // Validate that the user can edit the questionnaire and that it exists
        //check that the user can edit the questionnaire and that the questionnaire and the question exists
        $questionnaire_count = Questionnaire::where([
            ['slug', '=', $questionnaire],
            ['creator_id', '=', Auth::user()->id]
        ])->count();
        if(Gate::allows('edit_questionnaires') && $questionnaire_count == 1){
            // Get the questionnaire and check that the question exists
            $questionnaire = Questionnaire::where([
                ['slug', '=', $questionnaire],
                ['creator_id', '=', Auth::user()->id]
            ])->first();
            $question_count = Question::where([
                ['slug', '=', $question],
                ['questionnaire_id', '=', $questionnaire->id]
            ])->count();
            // check that it exists
            if($question_count == 1){
                //if the question exists fetch it and the scale to send to the edit page
                $question = Question::where([
                    ['slug', '=', $question],
                    ['questionnaire_id', '=', $questionnaire->id]
                ])->first();
                // get the scale

                //validate the form
                $this->validate($request, [
                    'question_id' => 'required|alpha_dash',
                    'start' => 'required|min:3|max:255',
                    'end' => 'required|min:3|max:255',
                    'positions' => 'required|integer|min:3|max:10',
                ]);

                // check that the question id matches the slug
                $scale_count = Scale::where([
                    ['id', '=', $id],
                    ['question_id', '=', $question->id]
                ])->count();

                if($scale_count == 1 && $request->get('question_id') == $question->slug){
                    // now update the scale
                    $scale = Scale::where([
                        ['id', '=', $id],
                        ['question_id', '=', $question->id]
                    ])->first();

                    $input = $request->all();
                    //change the question_id to be the id not the slug
                    $input['question_id'] = $question->id;

                    $scale->update($input);


                    // Send the user to the questions page
                    return redirect('/questionnaires/' . $questionnaire->slug)->withSuccess('The scale for "' . $question->question . '" has been set.');
                } else {
                    // if not return the users to the question
                    return redirect('/questionnaires/' . $questionnaire->slug)->withError('The scale could not be found.');
                }

            } else {
                return redirect('/questionnaires/' . $questionnaire->slug)->withError('That question does not exist.');
            }
        } else {
            return redirect('/questionnaires/' . $questionnaire)->withError('You are unable to edit this questionnaire.');
        }
    }
}
