<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Route::group(['middle' => ['web']], function() {

    Route::auth();

    Route::get('/home', 'ResponseController@index');
    Route::resource('/respond', 'ResponseController');
    Route::get('/respond/{questinnaire}/start', 'ResponseController@start_ques');
    Route::patch('/respond/{questionnaire}/questions', 'ResponseController@response');
    Route::get('/respond/{questinnaire}/finished', 'ResponseController@finished');

    Route::resource('/admin/users', 'UsersController');
    // Routes to reset the passwords for users
    Route::get('/admin/users/{user}/reset_password', 'UsersController@reset_form');
    Route::patch('/admin/users/{user}/password', 'UsersController@reset_password');


    Route::resource('/questionnaires', 'QuestionnairesController');
    Route::post('/questionnaires/open', 'QuestionnairesController@open_questionnaire');
    Route::post('/questionnaires/close', 'QuestionnairesController@close_questionnaire');
    Route::get('/questionnaires/{questionnaire}/responses', 'QuestionnairesController@responses');

    // Questions route
    Route::resource('/questionnaires/{questionnaire}/questions', 'QuestionsController');

    //options route
    Route::resource('/questionnaires/{questionnaire}/questions/{question}/options', 'OptionsController');

    //Scale route
    Route::resource('/questionnaires/{questionnaire}/questions/{question}/scales', 'ScalesController');


});