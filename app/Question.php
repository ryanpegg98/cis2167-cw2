<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
      'questionnaire_id',
      'question',
      'slug',
      'type',
      'layout',
    ];

    public function questionnaire()
    {
        return $this->belongsTo(Questionnaire::class);
    }
}
