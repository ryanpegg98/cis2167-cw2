<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = [
        'question_id',
        'option'
    ];

    /**
     * Get the question it belongs to
     */

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

}
