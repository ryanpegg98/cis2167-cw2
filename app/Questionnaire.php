<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    // Variables for the columns in the database which the user can define
    protected $fillable = [
        'title',
        'slug',
        'description',
        'agreement',
        'layout',
        'status',
        'creator_id'
    ];

    /**
     * Get who the questionnaire belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function user(){
        return $this->belongsTo(User::class);
    }
}
