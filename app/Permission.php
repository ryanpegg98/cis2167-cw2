<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /*
     * Get all of the roles it is associated with
     */

    public function roles(){
        return $this->belongsToMany(Role::class);
    }
}
