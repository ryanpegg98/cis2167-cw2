<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respondent extends Model
{
    // Add in the fillables to allow responses to be submitted to the database
    protected $fillable = [
        'questionnaire_id',
    ];
}
