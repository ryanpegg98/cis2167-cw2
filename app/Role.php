<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /*
     *  Get the permissions for the role
     */

    public function permissions() {
        return $this->belongsToMany(Permission::class);
    }

    /*
     *  Allow a permission to be added to a role
     */

    public function givePermissionTo(Permission $permission) {
        return $this->permission()->sync($permission);
    }
}
