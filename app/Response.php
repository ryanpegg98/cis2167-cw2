<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    // Add in the column names that will allow responses to be stored
    protected $fillable =[
        'respondent_id',
        'question_id',
        'response',
    ];
}
