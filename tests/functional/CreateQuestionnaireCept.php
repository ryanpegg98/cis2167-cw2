<?php 
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('Create a questionnaire');

// Login using an admin
Auth::loginUsingId(1);

// Start from the beginning
$I->amGoingTo('Direct the user to the create questionnaires page');
$I->amOnPage('/questionnaires');
$I->dontSee('Test Questionnaire', 'a');
$I->amOnPage('/questionnaires/create');
//Check that the user is on the correct page
$I->seeCurrentUrlEquals('/questionnaires/create');
$I->see('Create Questionnaire', 'h1');

//Now lets test the validation
$I->amGoingTo('Submit the form with empty fields');
//Now submit the form with empty fields
$I->submitForm('#createquestionnaire', []);

//Check the page is correct
$I->dontSeeCurrentUrlEquals('/questionnaires');

//Look for the error messages
$I->see('The title field is required.');
$I->see('The slug field is required.');
$I->see('The description field is required.');

// Add data to test the form with
$I->haveRecord('questionnaires', [
    'id' => 900,
    'title' => 'Test Questionnaire',
    'slug' => 'test-questionnaire',
    'description' => 'This is just a test questionnaire',
    'status' => 0,
    'layout' => 1
]);

$I->amGoingTo('Try and enter data that already exixts');
$I->seeCurrentUrlEquals('/questionnaires/create');
$I->submitForm('#createquestionnaire', [
    'title' => 'Test Questionnaire',
    'slug' => 'test-questionnaire',
    'description' => 'This is just a test questionnaire',
    'layout' => 1
]);

// the description and layout can be the same
$I->see('The title has already been taken.');
$I->see('The slug has already been taken.');

$I->amGoingTo('See if it will create a questionnaire with correct value');
$I->seeCurrentUrlEquals('/questionnaires/create');
$I->see('Create Questionnaire', 'h1');

//Submit the form
$I->submitForm('#createquestionnaire', [
    'title' => 'A Test Questionnaire',
    'slug' => 'a-test-questionnaire',
    'description' => 'This is just a test questionnaire',
    'layout' => 1
]);

$I->seeCurrentUrlEquals('/questionnaires');
// I should see the questionnaires page with the flash message
$I->see('Questionnaires', 'h1');
$I->see('"A Test Questionnaire" has been created.');
$I->see('A Test Questionnaire', 'a');

//END OF TEST
