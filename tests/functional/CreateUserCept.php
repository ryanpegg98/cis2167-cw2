<?php 
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('Create a User');

// Log the user in with the admin account
Auth::loginUsingId(1);

//When
$I->amOnPage('/');
$I->see('Users', 'a');

//And
$I->click('Users', 'a');

//Then
$I->seeCurrentUrlEquals('/admin/users');
$I->see('Users', 'h1');
$I->see('Create User', 'a');
$I->dontSee('Test User', 'a');

// Then add a user starts
$I->click('Create User', 'a');

// And the page is
$I->seeCurrentUrlEquals('/admin/users/create');
//And I see the content
$I->see('Create User', 'h1');

// Then enter incorrect data
$I->submitForm('#createuser', [
    'name' => 'Test User',
    'email' => 'ryan@example.com',
    'password' => 'password'
]);

// Check that it has returned error messages
$I->seeCurrentUrlEquals('/admin/users/create');
// And that there are error messages
$I->see('The email has already been taken.');

// Now repeat it to test that it checks for the name
$I->submitForm('#createuser', [
    'name' => '',
    'email' => 'testuser@example.ry',
    'password' => 'password'
]);

// Check that it has returned error messages
$I->seeCurrentUrlEquals('/admin/users/create');
// And that there are error messages
$I->see('The name field is required.');

// Now repeat it to test that it checks for the password
$I->submitForm('#createuser', [
    'name' => 'Test User',
    'email' => 'testuser@example.ry',
    'password' => ''
]);

// Check that it has returned error messages
$I->seeCurrentUrlEquals('/admin/users/create');
// And that there are error messages
$I->see('The password field is required.');

// Now repeat it to test that it checks for the email
$I->submitForm('#createuser', [
    'name' => 'Test User',
    'email' => '',
    'password' => 'password'
]);

// Check that it has returned error messages
$I->seeCurrentUrlEquals('/admin/users/create');
// And that there are error messages
$I->see('The email field is required.');

//Check the password is the correct length
$I->submitForm('#createuser', [
    'name' => 'Test User',
    'email' => 'testuser@example.ry',
    'password' => 'pass'
]);

// Check that it has returned error messages
$I->seeCurrentUrlEquals('/admin/users/create');
// And that there are error messages
$I->see('The password must be at least 6 characters.');

//Now it is time to see if the data is stored in the system
$I->submitForm('#createuser', [
    'name' => 'Test User',
    'email' => 'testuser@example.ry',
    'password' => 'password'
]);

// See the url is equal to the users
$I->seeCurrentUrlEquals('/admin/users');
// And the heading is present
$I->see('Users', 'h1');
$I->see('"Test User" has been created.');

// See if the user can now be seen
$I->see('Test User', 'a');

// Time to see if a basic user can create a user

Auth::loginUsingId(2);

// I am on the home page
$I->amOnPage('/');
$I->see('Questionnaires');
// Check the admin has been logged out
$I->dontSee('Ryan Pegg');

// try and access users page
$I->amOnPage('/admin/users');
//make sure the url is not the users page
$I->dontSeeCurrentUrlEquals('/admin/users');
$I->dontSee('Users');

// try and access users create page
$I->amOnPage('/admin/users/create');
//make sure the url is not the users page
$I->dontSeeCurrentUrlEquals('/admin/users/create');
$I->dontSee('Create User');