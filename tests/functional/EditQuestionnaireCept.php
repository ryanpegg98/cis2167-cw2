<?php 
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('Update a questionnaire');

// Login to the admin account
Auth::loginUsingId(1);

//Give a record to the database
$I->haveRecord('questionnaires', [
    'title' => 'Test Questionnaire',
    'slug' => 'test-questionnaire-edit',
    'description' => 'This is just a test questionnaire.',
    'layout' => 1,
    'creator_id' => 1
]);

$I->amGoingTo('navigate the user to the questionnaires page');
//put the user on the home page
$I->amOnPage('/');
$I->see('My Questionnaires', 'a');
$I->click('My Questionnaires' , 'a');
// Check they have landed on the correct page
$I->seeCurrentUrlEquals('/questionnaires');
$I->see('Questionnaires','h1');

//See the questionnaire
$I->see('Test Questionnaire', 'a');
$I->click('Test Questionnaire', 'a');
// Now click the edit button
$I->see('Edit', [
    'name' => 'edittest-questionnaire-edit'
]);
// Click the button
$I->click('Edit', [
    'name' => 'edittest-questionnaire-edit'
]);

// Now check that the form validates for no entry
$I->amGoingTo('Check the form validates the input from the user');
$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-edit/edit');
$I->see('Edit - Test Questionnaire', 'h1');
$I->fillField('title', '');
$I->fillField('slug', '');
$I->fillField('description', '');
// Submit the form
//$I->see('Edit Questionnaire', 'input');
$I->click('Edit Questionnaire', 'input');

// see that the page is still the edit page
$I->dontSeeCurrentUrlEquals('/questionnaires');
$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-edit/edit');

// Look for the error messages
$I->see('The title field is required.');
$I->see('The slug field is required.');
$I->see('The description field is required.');

$I->amGoingTo('Check to make sure that it checks the uniqueness of the input');

// Give another record with certain credentials
$I->haveRecord('questionnaires', [
    'title' => 'Test Unique Questionnaire',
    'slug' => 'test-questionnaire-unique',
    'description' => 'This is just a test questionnaire.',
    'layout' => 1,
    'creator_id' => 2
]);

// check the URL is correct
$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-edit/edit');

//Fill with the predefined values
$I->fillField('title', 'Test Unique Questionnaire');
$I->fillField('slug', 'test-questionnaire-unique');

//submit the form
//$I->see('Edit Questionnaire', 'input');
$I->click('Edit Questionnaire', 'input');

//See that the url equals certain value
$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-edit/edit');

// Look for the error messages
$I->see('The title has already been taken.');
$I->see('The slug has already been taken.');

$I->amGoingTo('Apply the changes');

// now the data should be chanaged in the database
$I->fillField('title', 'Test Edited Questionnaire');
$I->fillField('slug', 'test-questionnaire-edited');
$I->fillField('description', 'This is the updated information');

//submit the form
//$I->see('Edit Questionnaire', 'input');
$I->click('Edit Questionnaire', 'input');

// The user should be on the questionnaire page
$I->seeCurrentUrlEquals('/questionnaires');
$I->see('Questionnaires', 'h1');
$I->see('Test Edited Questionnaire', 'a');
$I->dontSee('Test Questionnaire', 'a');

// END OF TEST