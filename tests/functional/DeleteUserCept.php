<?php 
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('Delete a user');

// Log the user in using the admin ID
Auth::loginUsingId(1);

// Give records to the database
$I->haveRecord('users', [
    'id' => 900,
    'name' => 'Test User',
    'email' => 'testuser@test.com',
    'password' => bcrypt('password')
]);

/**
 * First step is to navigate to the users page
 */
$I->amGoingTo('Navigate to the users page');
$I->amOnPage('/');
$I->see('Users', 'a');
$I->click('Users', 'a');
$I->see('Users', 'h1');

/**
 * Now find the user can submit the delete form
 */

$I->amGoingTo('Find the user and submit the delete form');
$I->see('Test User', 'a');
$I->click('Test User', 'a');
$I->submitForm('#deleteuser900', []);

/**
 * The user should now be redirected and a message should be displayed
 */

$I->seeCurrentUrlEquals('/admin/users');
$I->see('"Test User" has been deleted.');
$I->dontSee('Test User','a');

// END OF THE TEST

