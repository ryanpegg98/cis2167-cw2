<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('Delete a questionnaire');

// Log the user in using the admin ID
Auth::loginUsingId(1);

// Give records to the database
$I->haveRecord('questionnaires', [
    'title' => 'Test Questionnaire',
    'slug' => 'test-questionnaire-edit',
    'description' => 'This is just a test questionnaire.',
    'layout' => 1,
    'creator_id' => 1
]);

/**
 * First step is to navigate to the questionnaires page
 */
$I->amGoingTo('Navigate to the questionnaires page');
$I->amOnPage('/');
$I->see('My Questionnaires', 'a');
$I->click('My Questionnaires', 'a');
$I->see('Questionnaires', 'h1');

/**
 * Now find the user can submit the delete form
 */

$I->amGoingTo('Find the questionnaire and submit the delete form');
$I->see('Test Questionnaire', 'a');
$I->click('Test Questionnaire', 'a');
$I->submitForm('#deletetest-questionnaire-edit', []);

/**
 * The user should now be redirected and a message should be displayed
 */

$I->seeCurrentUrlEquals('/questionnaires');
$I->see('"Test Questionnaire" has been deleted.');
$I->dontSee('Test Questionnaire','a');

// END OF THE TEST

