<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('Create Question on a questionnaire');

// Log the test in using the admin account
Auth::loginUsingId(1);

// give the database sample data to use
$I->haveRecord('questionnaires', [
    'title' => 'Test Questionnaire',
    'slug' => 'test-questionnaire-view',
    'description' => 'This is just a test questionnaire.',
    'layout' => 1,
    'creator_id' => 1
]);

// Get the questionnaire
$questionnaire = $I->grabRecord('questionnaires', [
   'slug' => 'test-questionnaire-view'
]);

// Start the user on the home page
$I->amGoingTo('Navigate to the questionnaire');
$I->amOnPage('/');

$I->see('My Questionnaires', 'a');
$I->click('My Questionnaires', 'a');

// Find the questionnaire
$I->seeCurrentUrlEquals('/questionnaires');
$I->see('Questionnaires', 'h1');

$I->see('Test Questionnaire','a');
$I->click('Test Questionnaire', 'a');

$I->see('Questions', [
    'name' => 'viewtest-questionnaire-view'
]);

// Go to page with all questions
$I->click('Questions', [
    'name' => 'viewtest-questionnaire-view'
]);

//check that the questionnaire
$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view');

$I->see('Test Questionnaire', 'h1');

$I->amGoingTo('Try to access the add question form');
$I->see('Add Question', 'a');
$I->click('Add Question', 'a');

//Check the url is correct
$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view/questions/create');
$I->see('Add Question', 'h1');

$I->amGoingTo('Test the form validation');

$I->submitForm('#createquestion', [
    'question' => '',
    'slug' => '',
    'type' => 0
]);

//check for the errors
$I->see('The question field is required.');
$I->see('The slug field is required.');

// try again with a value that is the same as another
$I->haveRecord('questions', [
   'questionnaire_id' => $questionnaire->id,
    'question' => 'Same question test',
    'slug' => 'same-as-test'
]);

// Look for the same value
$I->amGoingTo('Check that it validates the same values for question and slug');

$I->submitForm('#createquestion', [
    'question' => 'Same question test',
    'slug' => 'same-as-test',
    'type' => 0
]);

//check the url is the same
$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view/questions/create');

//check to see the error messages
$I->see('The question has already been taken.');
$I->see('The slug has already been taken.');

//now submit the form to create a text question
$I->amGoingTo('Create a question for the questionnaire');

$I->submitForm('#createquestion', [
    'question' => 'Text question?',
    'slug' => 'text-basic-question',
    'type' => 1
]);

//check the url equals the questionnaire
$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view');
$I->see('"Text question?" has been created.');

$I->see('Add Question', 'a');
$I->click('Add Question', 'a');

//Check the url is correct
$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view/questions/create');
$I->see('Add Question', 'h1');

$I->amGoingTo('Test that a paragraph can be created');

$I->submitForm('#createquestion', [
    'question' => 'Paragraph question',
    'slug' => 'para-question',
    'type' => 2
]);

//check the url equals the questionnaire
$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view');
$I->see('"Paragraph question" has been created.');

$I->see('Add Question', 'a');
$I->click('Add Question', 'a');

//Check the url is correct
$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view/questions/create');
$I->see('Add Question', 'h1');

$I->amGoingTo('Test that a options can be created');

$I->submitForm('#createquestion', [
    'question' => 'Options Listed Question',
    'slug' => 'options-listed-question',
    'type' => 3
]);

$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view/questions');

$I->selectOption('layout', 2);
$I->click('Add Question', 'input');

$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view/questions/options-listed-question');
$I->see('Options Listed Question', 'h1');
$I->see('Options:', 'h3');
$I->see('Add Option');

$I->amGoingTo('Add an option to the question');

$I->fillField('option', 'Option 1');
$I->click('Add Option', 'button');

// Check the url is the same
$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view/questions/options-listed-question');
$I->see('Options Listed Question', 'h1');
$I->see('Options:', 'h3');
$I->see('"Option 1" has been added.');
$I->see('Option 1');

// now go back to the questionnaire
$I->amGoingTo('Go back to the questionnaire page and create a scale for the questionnaire');

$I->see('Test Questionnaire', 'a');
$I->click('Test Questionnaire', 'a');

$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view');

$I->see('Add Question', 'a');
$I->click('Add Question', 'a');

//Check the url is correct
$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view/questions/create');
$I->see('Add Question', 'h1');

$I->amGoingTo('Test that a scale question can be created');

$I->submitForm('#createquestion', [
    'question' => 'scale Listed Question',
    'slug' => 'scale-listed-question',
    'type' => 4
]);

$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view/questions');

$I->selectOption('layout', 2);
$I->click('Add Question', 'input');

$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view/questions/scale-listed-question/scales');
$I->see('Scale Listed Question', 'h1');
$I->see('Scale:', 'h3');

// Add in the setting of the scale
$I->fillField('start', 'Sad');
$I->fillField('end', 'Happy');
$I->fillField('positions', '5');

$I->click('Set Scale');

$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view');

$I->see('The scale for "Scale Listed Question" has been set.');

$I->amGoingTo('Add another options question ');

$I->see('Add Question', 'a');
$I->click('Add Question', 'a');

//Check the url is correct
$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view/questions/create');
$I->see('Add Question', 'h1');

$I->amGoingTo('Test that a options can be created');

$I->submitForm('#createquestion', [
    'question' => 'Options Drop Down Question',
    'slug' => 'options-dropdown-question',
    'type' => 3
]);

$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view/questions');

$I->selectOption('layout', 1);
$I->click('Add Question', 'input');

$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view/questions/options-dropdown-question');
$I->see('Options Drop Down Question', 'h1');
$I->see('Options:', 'h3');
$I->see('Add Option');

$I->amGoingTo('Add an option to the question');

$I->fillField('option', 'Option 1');
$I->click('Add Option', 'button');

// Check the url is the same
$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view/questions/options-dropdown-question');
$I->see('Options Drop Down Question', 'h1');
$I->see('Options:', 'h3');
$I->see('"Option 1" has been added.');
$I->see('Option 1');

$I->see('Test Questionnaire', 'a');
$I->click('Test Questionnaire', 'a');

$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view');

$I->see('Add Question', 'a');
$I->click('Add Question', 'a');

//Check the url is correct
$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view/questions/create');
$I->see('Add Question', 'h1');

$I->amGoingTo('Test that a different scale question can be created');

$I->submitForm('#createquestion', [
    'question' => 'scale Drop Down Question',
    'slug' => 'scale-dropdown-question',
    'type' => 4
]);

$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view/questions');

$I->selectOption('layout', 2);
$I->click('Add Question', 'input');

$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view/questions/scale-dropdown-question/scales');
$I->see('Scale Drop Down Question', 'h1');
$I->see('Scale:', 'h3');

// Add in the setting of the scale
$I->fillField('start', 'Good');
$I->fillField('end', 'Bad');
$I->fillField('positions', '5');

$I->click('Set Scale');

$I->seeCurrentUrlEquals('/questionnaires/test-questionnaire-view');

$I->see('The scale for "Scale Drop Down Question" has been set.');


// END OF TEST