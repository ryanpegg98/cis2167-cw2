<?php 
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('Edit a user');

//Login as the admin
Auth::loginUsingId(1);

/**
 * Give the user record to change
 */
$I->haveRecord('users', [
    'id' => 900,
    'name' => 'Test User',
    'email' => 'testuser@test.com',
    'password' => bcrypt('password')
]);

/**
 * Fetch the user to get the id for future use
 */

$user = $I->grabRecord('users', [
    'email' => 'testuser@test.com'
]);


// Start on the home page
$I->amOnPage('/');
$I->see('Users', 'a');
$I->see('Ryan Pegg');// Check they are the admin

//Then go to the users page
$I->click('Users', 'a');

// Now find the user
$I->seeCurrentUrlEquals('/admin/users');
$I->see($user->name, 'a');
$I->dontSee('Edit Test User');
//Click the users name
$I->click($user->name, 'a');
$I->see('Edit', 'a');
$I->click('Edit', [
    'name' => 'edit900'
]);

// See that the pages has changes
$I->seeCurrentUrlEquals('/admin/users/900/edit');
// See if it can see the users name
$I->see('Edit - ' . $user->name, 'h1');

// Change the name to "Edit Test User"
$I->fillField('name', 'Edit Test User');
$I->see('Edit', 'input');
$I->click('Edit', 'input');

//  See if the page is the users page
$I->seeCurrentUrlEquals('/admin/users');
$I->see('Edit Test User');

// Check the message has been sent
$I->see('"Edit Test User" has been updated.');

$I->amGoingTo('Check user cannot have an email that is already being used.');

// First go back to the edit page
$I->click('Edit Test User', 'a');
$I->click('Edit', [
    'name' => 'edit900'
]);

//see the current url equal the edit page
$I->seeCurrentUrlEquals('/admin/users/900/edit');

$I->see('Edit - Edit Test User');

//Try and enter the admin email address
$I->fillField('email', 'ryan@example.com');
$I->click('Edit', 'input');

// See if the URL equals the edit page
$I->dontSeeCurrentUrlEquals('/admin/users');
$I->seeCurrentUrlEquals('/admin/users/900/edit');

//Check for the error message
$I->see('The email has already been taken.');

// Check that the form check for a valid email address
$I->fillField('email', 'emailaddress');
$I->click('Edit', 'input');

//check that the site is on the correct page
$I->dontSeeCurrentUrlEquals('/admin/users');
$I->seeCurrentUrlEquals('/admin/users/900/edit');

//Check that the message can be see
$I->see('The email must be a valid email address.');

$I->amGoingTo('Enter a blank name and email to make sure they are required');

//Fill the fileds
$I->fillField('name', '');
$I->fillField('email', '');

$I->click('Edit', 'input');

// Should be on the correct page
$I->dontSeeCurrentUrlEquals('/admin/users');
$I->seeCurrentUrlEquals('/admin/users/900/edit');

// Should see the error messages
$I->see('The name field is required.');
$I->see('The email field is required.');

// END OF THE TEST