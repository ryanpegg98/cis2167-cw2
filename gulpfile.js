var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    // Complie the CSS files into one
    mix.sass(
        'app.scss', //The base file
        'public/css', //Where the files should go
        {includePaths: ['vendor/bower_components/foundation/scss']}
    );

    // Compile JavaScript files
    mix.scripts(
        ['vendor/modernizr.js', 'vendor/jquery.js', 'foundation.min.js'], // The files to mix
        'public/js/app.js', // Where they will be saved
        'vendor/bower_components/foundation/js/' //where the source files are stored
    );

});
