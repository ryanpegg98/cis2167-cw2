<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Role;
use App\Permission;
use App\Questionnaire;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');// This stops it checking for the foreign keys
        Model::unguard();// Allow us to truncate the tables with relations

        User::truncate();
        Role::truncate();
        Permission::truncate();

        Model::reguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');//Re-enable the checks

        /**
         * Run any seeders before the factories
         */
        $this->call(UserTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(PermissionRolesTableSeeder::class);

        /**
         * Now run the factories for auto-generated content
         */
        factory(Questionnaire::class, 1)->create();
        factory(User::class, 10)->create();

        /**
         * This seeder is based on the results from the factory
         */

        $this->call(RoleUsersTableSeeder::class);
        $this->call(QuestionTableSeeder::class);
        $this->call(OptionsTableSeeder::class);
        $this->call(ScalesTableSeeder::class);

    }
}
