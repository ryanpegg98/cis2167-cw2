<?php

use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('options')->insert([
            [
                'id' => 1,
                'question_id' => 3,
                'option' => 'This is an option',
            ], [
                'id' => 2,
                'question_id' => 3,
                'option' => 'This is another option',
            ], [
                'id' => 3,
                'question_id' => 3,
                'option' => 'And a third option',
            ], [
                'id' => 4,
                'question_id' => 4,
                'option' => 'Option 1',
            ], [
                'id' => 5,
                'question_id' => 4,
                'option' => 'Option 2',
            ], [
                'id' => 6,
                'question_id' => 4,
                'option' => 'Option 3',
            ],
        ]);
    }
}
