<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'id' => 1,
                'name' => 'see_adminnav',
                'label' => 'Can See Admin Nav',
            ], [
                'id' => 2,
                'name' => 'see_users',
                'label' => 'Can See Users',
            ], [
                'id' => 3,
                'name' => 'edit_users',
                'label' => 'Can Edit Users',
            ],  [
                'id' => 4,
                'name' => 'create_users',
                'label' => 'Can Create Users',
            ],  [
                'id' => 5,
                'name' => 'delete_users',
                'label' => 'Can Delete Users',
            ], [
                'id' => 6,
                'name' => 'assign_roles',
                'label' => 'Can Assign Roles',
            ], [
                'id' => 7,
                'name' => 'reset_passwords',
                'label' => 'Can Reset Passwords',
            ], [
                'id' => 8,
                'name' => 'create_questionnaires',
                'label' => 'Can Create Questionnaires',
            ],  [
                'id' => 9,
                'name' => 'edit_questionnaires',
                'label' => 'Can Edit Questionnaires',
            ],  [
                'id' => 10,
                'name' => 'delete_questionnaires',
                'label' => 'Can Delete Questionnaires',
            ],
        ]);
    }
}
