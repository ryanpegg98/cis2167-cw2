<?php

use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            [
                'id' => 1,
                'questionnaire_id' => 1,
                'question' => 'Text Question?',
                'slug' => 'text-question',
                'type' => 1,
                'layout' => 0,
            ], [
                'id' => 2,
                'questionnaire_id' => 1,
                'question' => 'Paragraph Question?',
                'slug' => 'paragraph-question',
                'type' => 2,
                'layout' => 0,
            ], [
                'id' => 3,
                'questionnaire_id' => 1,
                'question' => 'Options drop-down Question?',
                'slug' => 'option-drop-down-question',
                'type' => 3,
                'layout' => 1,
            ], [
                'id' => 4,
                'questionnaire_id' => 1,
                'question' => 'Options listed Question?',
                'slug' => 'option-listed-question',
                'type' => 3,
                'layout' => 2,
            ], [
                'id' => 5,
                'questionnaire_id' => 1,
                'question' => 'Scale drop-down Question?',
                'slug' => 'scale-drop-down-question',
                'type' => 4,
                'layout' => 1,
            ], [
                'id' => 6,
                'questionnaire_id' => 1,
                'question' => 'Scale listed Question?',
                'slug' => 'scale-listed-question',
                'type' => 4,
                'layout' => 2,
            ],
        ]);
    }
}
