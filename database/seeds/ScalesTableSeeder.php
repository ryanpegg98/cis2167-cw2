<?php

use Illuminate\Database\Seeder;

class ScalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('scales')->insert([
            [
                'id' => 1,
                'question_id' => 5,
                'start' => 'Sad',
                'end' => 'Happy',
                'positions' => 3,
            ], [
                'id' => 2,
                'question_id' => 6,
                'start' => 'Poor',
                'end' => 'Excellent',
                'positions' => 5,
            ],
        ]);
    }
}
