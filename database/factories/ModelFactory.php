<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});


/**
 *  Factory to create a test questionnaire for the admin
 */

$factory->define(App\Questionnaire::class, function (Faker\Generator $faker){
    return [
      'title' => $faker->sentence,
      'slug' => $faker->word,
      'description' => $faker->paragraph,
      'agreement' => $faker->paragraph,
      'layout' => 1,
      'status' => 0,
      'creator_id' => 1,
    ];
});