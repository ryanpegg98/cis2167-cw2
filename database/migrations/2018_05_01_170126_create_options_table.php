<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create the options table
        Schema::create('options', function (Blueprint $table){
           $table->increments('id');
           $table->integer('question_id')->unsigned();
           $table->string('option');
           $table->timestamps();
           //foreign key for the question_id
           $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // delete the options table
        Schema::drop('options');
    }
}
