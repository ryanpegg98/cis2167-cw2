 <?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create questions table
        Schema::create('questions', function(Blueprint $table){
            $table->increments('id');
            $table->integer('questionnaire_id')->unsigned();
            $table->string('question');
            $table->string('slug');
            $table->integer('type');
            $table->integer('layout')->default(0);
            $table->timestamps();
            //foreign key for the questionnaire_id
            $table->foreign('questionnaire_id')->references('id')->on('questionnaires')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //remove the roles table
        Schema::drop('questions');
    }
}
