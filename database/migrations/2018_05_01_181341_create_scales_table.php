<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scales', function(Blueprint $table){
            $table->increments('id');
            $table->integer('question_id')->unsigned()->unique();
            $table->string('start');
            $table->string('end');
            $table->integer('positions')->unsigned();
            $table->timestamps();
            //foreign key for the question_id
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //remove the table
        Schema::drop('scales');
    }
}
