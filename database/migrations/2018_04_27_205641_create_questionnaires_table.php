<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionnairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create questionnaires table
        Schema::create('questionnaires', function (Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->longText('description')->nullable();
            $table->longText('agreement')->nullable();
            $table->integer('layout');
            $table->integer('status');
            $table->integer('creator_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Remove the questionnaires table
        Schema::drop('questionnaires');
    }
}
