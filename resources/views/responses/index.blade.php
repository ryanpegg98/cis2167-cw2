@extends('layouts.app')

@section('title', 'Questionnaires')

@section('breadcrumbs')
    <li role="menuitem" class="current"><a href="/">Home</a></li>
@endsection

@section('content')

    <div class="row">
        <div class="small-12 columns">
            <h1>All Questionnaires</h1>
        </div>
    </div>
    <div class="row">
        @include('errors.messages')
    </div>
    <div class="row">
        <div class="small-12 columns dataBox">
            @if(isset($questionnaires))
                @if(count($questionnaires) > 0)
                    <ul class="accordion" data-accordion>
                        @foreach($questionnaires as $questionnaire)
                            <li class="accordion-navigation">
                                <a href="#take{{ $questionnaire->slug }}">{{ $questionnaire->title }}</a>
                                <div class="content small-text-center large-text-left" id="take{{ $questionnaire->slug }}">
                                    <a href="/respond/{{ $questionnaire->slug }}" class="button tiny">
                                        View Questionnaire
                                    </a>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                @else
                    <div class="alert-box warning">
                        <i class="fas fa-exclamation-triangle"></i> There are no questionnaires available
                    </div>
                @endif
            @else
                <div class="alert-box alert">
                    <i class="fas fa-times"></i> No questionnaires have been found
                </div>
            @endif
        </div>
    </div>

@endsection
