@extends('layouts.app')

@section('title', $questionnaire->title)

@section('breadcrumbs')
    <li role="menuitem"><a href="/">Home</a></li>
    <li role="menuitem" class="current"><a href="/respond/{{ $questionnaire->slug }}">{{ $questionnaire->title }}</a></li>
@endsection

@section('content')

    <div class="row small-text-center">
        <div class="small-12 large-11 columns large-text-left">
            <h1>{{ $questionnaire->title }}</h1>
            <p>Created By: {{ $questionnaire['name'] }}</p>
            <br /><br />
        </div>
        <div class="small-12 large-1 columns large-text-right">
            @if(strlen($questionnaire->agreement) > 0)
                <a href="/respond/{{ $questionnaire->slug }}/start" onclick="return confirm('Do you agree with the ethical agreement?')" class="button success tiny topButton">
            @else
                <a href="/respond/{{ $questionnaire->slug }}/start" class="button success tiny topButton">
            @endif
                Respond
            </a>
        </div>
    </div>
    <div class="row">
        @include('errors.messages')
    </div>
    <div class="row">
        @if(strlen($questionnaire->agreement) > 0)
            <div class="small-12 large-6 columns">
                <h3>Ethical Agreement:</h3>
                <div class="panel callout">
                    {{ $questionnaire->agreement }}
                </div>
                <p>By clicking the "Respond" button you agree to the above ethical agreement.</p>
            </div>
            <div class="small-12 large-6 columns">
        @else
            <div class="small-12 columns">
        @endif
                <h3>Description:</h3>
                <div class="panel">
                    {{ $questionnaire->description }}
                </div>
            </div>
    </div>

@endsection
