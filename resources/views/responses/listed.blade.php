@extends('layouts.app')

@section('title', $questionnaire->title)

@section('breadcrumbs')
    <li role="menuitem"><a href="/">Home</a></li>
    <li role="menuitem"><a href="/respond/{{ $questionnaire->slug }}">{{ $questionnaire->title }}</a></li>
    <li role="menuitem" class="current"><a href="/respond/{{ $questionnaire->slug }}/start">Start</a></li>
@endsection

@section('content')

    <div class="row small-text-center">
        <div class="small-12 columns large-text-left">
            <h1>{{ $questionnaire->title }}</h1>
            <p>Created By: {{ $questionnaire['name'] }}</p>
            <br /><br />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns dataBox">
            {!! Form::open(['action' => 'ResponseController@store', 'id' => 'submitresponse', 'onsubmit' => 'return confirm("Are you sure?")']) !!}
            {!! Form::hidden('questionnaire', $questionnaire->slug) !!}
            {!! Form::hidden('layout', 'listed') !!}
            @foreach($questions as $question)
                <div class="panel">
                    {!! Form::label($question->id, $question->question) !!}
                    <br />
                    @if($question->type == 1)
                        {!! Form::text($question->id) !!}
                    @elseif($question->type == 2)
                        {!! Form::textarea($question->id) !!}
                    @elseif($question->type == 3)
                        @if($question->layout == 1)
                            {!! Form::select($question->id, $question['options']) !!}
                        @elseif($question->layout == 2)

                            @foreach($question['options'] as $option)
                                <div class="panel callout">
                                    <?php
                                        $key = array_search($option, $question['options']);
                                    ?>
                                    {!! Form::radio($question->id, $key) !!}
                                    {!! Form::label($option) !!}
                                </div>
                            @endforeach
                        @endif
                    @elseif($question->type == 4)
                        @if($question->layout == 1)
                            {!! Form::select($question->id, $question['options']) !!}
                        @elseif($question->layout == 2)
                            @foreach($question['options'] as $option)
                                <div class="panel callout">
                                    <?php
                                    $key = array_search($option, $question['options']);
                                    ?>
                                    {!! Form::radio($question->id, $key) !!}
                                    @if(strlen($option) > 1)
                                        {!! Form::label($option) !!}
                                    @endif
                                </div>
                            @endforeach
                        @endif
                    @endif
                </div>
            @endforeach

                {!! Form::submit('Submit Response', ['class' => 'button success tiny right']) !!}

            {!! Form::close() !!}
        </div>
    </div>

@endsection
