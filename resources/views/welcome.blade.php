@extends('layouts.app')

@section('title', 'Questionnaires')

@section('breadcrumbs')
    <li role="menuitem" class="current"><a href="/">Home</a></li>
@endsection

@section('content')

    <div class="row">
        <div class="small-12 columns">
            <h1>All Questionnaires</h1>
        </div>
    </div>
    <div class="row">
        @include('errors.messages')
    </div>

@endsection
