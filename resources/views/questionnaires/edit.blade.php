@extends('layouts.app')

@section('title', 'Edit - ' . $questionnaire->title)

@section('breadcrumbs')
    <li role="menuitem"><a href="/">Home</a></li>
    <li role="menuitem"><a href="/questionnaires">Questionnaires</a></li>
    <li role="menuitem"><a href="/questionnaires/{{ $questionnaire->slug }}">{{ $questionnaire->title }}</a></li>
    <li role="menuitem" class="current"><a href="/questionnaires/{{ $questionnaire->slug }}/edit">Edit</a></li>
@endsection

@section('content')
    <div class="row small-text-center">
        <div class="small-12 large-text-left">
            <h1>Edit - {{ $questionnaire->title }}</h1>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            @include('errors.errorlist')
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            {!! Form::model($questionnaire, ['method' => 'PATCH', 'url' => '/questionnaires/' . $questionnaire->slug]) !!}

            {!! csrf_field() !!}

            @include('partials.questionnaires.form')

            {!! Form::submit('Edit Questionnaire', ['class' => 'button success tiny right']) !!}

            {!! Form::close() !!}
        </div>
    </div>
@endsection