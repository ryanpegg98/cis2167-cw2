@extends('layouts.app')

@section('title', 'Create Questionnaire')

@section('breadcrumbs')
    <li role="menuitem"><a href="/">Home</a></li>
    <li role="menuitem"><a href="/questionnaires">Questionnaires</a></li>
    <li role="menuitem" class="current"><a href="/questionnaires/create">Create</a></li>
@endsection

@section('content')
    <div class="row small-text-center">
        <div class="small-12 large-text-left">
            <h1>Create Questionnaire</h1>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            @include('errors.errorlist')
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            {!! Form::open(['url' => '/questionnaires', 'id' => 'createquestionnaire']) !!}

                {!! csrf_field() !!}

                @include('partials.questionnaires.form')

                {!! Form::submit('Create Questionnaire', ['class' => 'button success tiny right']) !!}

            {!! Form::close() !!}
        </div>
    </div>
@endsection