@extends('layouts.app')

@section('title', $question->question . ' - Scale')

@section('breadcrumbs')
    <li role="menuitem"><a href="/">Home</a></li>
    <li role="menuitem"><a href="/questionnaires">Questionnaires</a></li>
    <li role="menuitem"><a href="/questionnaires/{{ $questionnaire->slug }}">{{ $questionnaire->title }}</a></li>
    <li role="menuitem" class="unavailable"><a href="#">Questions</a></li>
    <li role="menuitem"><a href="/questionnaires/{{ $questionnaire->slug }}/questions/{{ $question->slug }}">{{ $question->question }}</a></li>
    <li role="menuitem" class="current"><a hre="#">Scale</a></li>
@endsection

@section('content')
    <div class="row small-text-center">
        <div class="small-12 columns large-text-left">
            <h1>{{ $question->question }}</h1>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            @include('errors.messages')
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns small-text-center large-text-left">
            @if($question->type == 4)
                <h3>Scale:</h3>

                <div class="row">
                    <div class="small-12 large-6 columns">
                        {!! Form::model($scale, ['method' => 'PATCH', 'url' => '/questionnaires/' . $questionnaire->slug . '/questions/' . $question->slug . '/scales/' . $scale->id ]) !!}

                        {!! Form::hidden('question_id', $question->slug) !!}

                        {!! Form::label('start', 'Start Value:') !!}
                        {!! Form::text('start') !!}

                        {!! Form::label('end', 'End Value:') !!}
                        {!! Form::text('end') !!}

                        {!! Form::label('positions', 'Number of Positions:') !!}
                        {!! Form::number('positions') !!}

                        {!! Form::submit('Set Scale', ['class' => 'button success tiny right']) !!}

                        {!! Form::close() !!}
                    </div>
                    <div class="small-12 large-6 columns">
                        @include('errors.errorlist')
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection