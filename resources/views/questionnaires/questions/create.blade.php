@extends('layouts.app')

@section('title', $questionnaire->title . ' - Questions')

@section('breadcrumbs')
    <li role="menuitem"><a href="/">Home</a></li>
    <li role="menuitem"><a href="/questionnaires">Questionnaires</a></li>
    <li role="menuitem"><a href="/questionnaires/{{ $questionnaire->slug }}">{{ $questionnaire->title }}</a></li>
    <li role="menuitem" class="unavailable"><a href="#">Questions</a></li>
    <li role="menuitem" class="current"><a href="/questionnaires/{{ $questionnaire->slug }}/questions/create">Create</a></li>
@endsection

@section('content')
    <div class="row small-text-center">
        <div class="small-12 columns large-text-left">
            <h1>Add Question</h1>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            @include('errors.messages')
        </div>
    </div>
    <div class="row">
        <div class="small-12 large-6 columns">
            {!! Form::open(['url' => '/questionnaires/' . $questionnaire->slug . '/questions', 'id' => 'createquestion']) !!}

                {!! csrf_field() !!}

                @include('partials.questions.form')

                {!! Form::submit('Add Question', ['class' => 'button success tiny right']) !!}


            {!! Form::close() !!}
        </div>
        <div class="small-12 large-6 columns">
            @include('errors.errorlist')
        </div>
    </div>
@endsection