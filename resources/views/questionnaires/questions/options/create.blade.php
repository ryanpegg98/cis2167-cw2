@extends('layouts.app')

@section('title', $question->question . ' - Options')

@section('breadcrumbs')
    <li role="menuitem"><a href="/">Home</a></li>
    <li role="menuitem"><a href="/questionnaires">Questionnaires</a></li>
    <li role="menuitem"><a href="/questionnaires/{{ $questionnaire->slug }}">{{ $questionnaire->title }}</a></li>
    <li role="menuitem" class="unavailable"><a href="#">Questions</a></li>
    <li role="menuitem"><a href="/questionnaires/{{ $questionnaire->slug }}/questions/{{ $question->slug }}">{{ $question->question }}</a></li>
    <li role="menuitem" class="current"><a href="#">Options</a></li>
@endsection

@section('content')
    <div class="row small-text-center">
        <div class="small-12 columns large-text-left">
            <h1>{{ $question->question }}</h1>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            @include('errors.messages')
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns small-text-center large-text-left">
            @if($question->type == 3)
                <h3>Options:</h3>

                <div class="row">
                    <div class="small-12 columns">
                        {!! Form::open(['url' => '/questionnaires/' . $questionnaire->slug . '/questions/' . $question->slug . '/options']) !!}

                        {!! Form::hidden('question', $question->slug) !!}

                        <div class="row">
                            <div class="small-12 large-10 columns">
                                {!! Form::text('option', null, ['placeholder' => 'Enter the option here']) !!}
                            </div>
                            <div class="small-12 large-2 columns large-text-right">
                                <button class="button success tiny" type="submit">
                                    <i class="fas fa-plus"></i> Add Option
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection