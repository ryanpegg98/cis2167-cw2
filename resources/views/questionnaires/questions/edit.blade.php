@extends('layouts.app')

@section('title', 'Edit - ' . $question->question)

@section('breadcrumbs')
    <li role="menuitem"><a href="/">Home</a></li>
    <li role="menuitem"><a href="/questionnaires">Questionnaires</a></li>
    <li role="menuitem"><a href="/questionnaires/{{ $questionnaire->slug }}">{{ $questionnaire->title }}</a></li>
    <li role="menuitem" class="unavailable"><a href="#">Questions</a></li>
    <li role="menuitem" class="current"><a href="/questionnaires/{{ $questionnaire->slug }}/questions/{{ $question->slug }}">{{ $question->question }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="small-12 columns">
            <h1>{{ $question->title }}</h1>
        </div>
    </div>

    <div class="row">
        <div class="small-12 large-6 columns">
            {!! Form::model($question, ['method' => 'PATCH', 'url' => '/questionnsires/' . $questionnaire->slug . '/questions/' . $question]) !!}
        </div>
    </div>
@endsection
