@extends('layouts.app')

@section('title', $questionnaire->title . ' - Responses')

@section('breadcrumbs')
    <li role="menuitem"><a href="/">Home</a></li>
    <li role="menuitem"><a href="/respond/{{ $questionnaire->slug }}">{{ $questionnaire->title }}</a></li>
    <li role="menuitem" class="current"><a href="/respond/{{ $questionnaire->slug }}/start">Start</a></li>
@endsection

@section('content')

    <div class="row small-text-center">
        <div class="small-12 columns large-text-left">
            <h1>{{ $questionnaire->title }}</h1>
            <br /><br />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns dataBox">
            <ul class="accordion" data-accordion>
                @foreach($questions as $question)
                    <li class="accordion-navigation">
                        <a href="#ques{{ $question->slug }}">{{ $question->question }}</a>
                        <div id="ques{{ $question->slug }}" class="content text-small-center">
                            @if($question->type == 1 || $question->type == 2)
                                <table role="grid">
                                    <thead>
                                        <tr>
                                            <th>Response</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($respondents as $respondent)
                                            <tr>
                                                @foreach($respondent['responses'] as $response)
                                                    @if($response->question_id == $question->id)
                                                        <td>{{ $response->response }}</td>
                                                    @endif
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @elseif($question->type == 3)
                                <table role="grid">
                                   <thead>
                                        <tr>
                                            <th>Option</th>
                                            <th>Response Rate</th>
                                        </tr>
                                   </thead>
                                   <tbody>
                                        @foreach($question['options'] as $option)
                                                    <tr>
                                                    <td>{{ $option->option }}</td>
                                                    <td>
                                                        <?php $rate = 0; ?>
                                                        @foreach($respondents as $respondent)
                                                            @foreach($respondent['responses'] as $response)
                                                                @if($response->question_id == $question->id && $response->response == $option->id)
                                                                    <?php
                                                                        $rate = $rate + 1;
                                                                    ?>
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                        {{ $rate }}
                                                    </td>
                                                </tr>
                                        @endforeach
                                   </tbody>
                                </table>
                            @elseif($question->type == 4)
                                    <table role="grid">
                                        <thead>
                                        <tr>
                                            <th>Scale Position</th>
                                            <th>Response Rate</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @for($i = 0; $i <= $question['options']->positions; $i++)
                                                <tr>
                                                    <td>
                                                        @if($i == 0)
                                                            {{ $question['options']->start }}
                                                        @elseif($i == $question['options']->positions)
                                                            {{ $question['options']->end }}
                                                        @else
                                                            *
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php $rate = 0; ?>
                                                        @foreach($respondents as $respondent)
                                                            @foreach($respondent['responses'] as $response)
                                                                @if($response->question_id == $question->id && $response->response == $i)
                                                                    <?php
                                                                    $rate = $rate + 1;
                                                                    ?>
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                        {{ $rate }}
                                                    </td>
                                                </tr>
                                            @endfor
                                        </tbody>
                                    </table>
                                @endif
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

@endsection
