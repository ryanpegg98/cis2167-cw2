@extends('layouts.app')

@section('title', 'Questionnaires')

@section('breadcrumbs')
    <li role="menuitem"><a href="/">Home</a></li>
    <li role="menuitem" class="current"><a href="/questionnaires">Questionnaires</a></li>
@endsection

@section('content')
    <div class="row small-text-center">
        <div class="small-12 large-6 columns large-text-left">
            <h1>Questionnaires</h1>
        </div>
        <div class="small-12 large-6 columns large-text-right">
            <a href="/questionnaires/create" class="button success tiny topButton">
                <i class="fas fa-plus"></i> Create Questionnaire
            </a>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            @include('errors.messages')
        </div>
    </div>
    <div class="row">
        @if(isset($questionnaires))
            @if(count($questionnaires))
                <div class="row dataBox">
                    <div class="small-12 columns">
                        <ul class="accordion" data-accordion>
                            @foreach($questionnaires as $questionnaire)
                                <li class="accordion-navigation">
                                    <a href="#questionnaire{{ $questionnaire->id }}">{{ $questionnaire->title }}</a>
                                    <div id="questionnaire{{ $questionnaire->id }}" class="content small-text-center">
                                        <div class="row">
                                            <div class="small-12 large-6 columns large-text-left">
                                                Description:
                                                <div class="panel">
                                                    {{ $questionnaire->description }}
                                                </div>
                                            </div>
                                            <div class="small-12 large-6 columns large-text-right">

                                                @if($questionnaire->status == 0)

                                                    <div class="alert-box large-text-center">
                                                        EDITABLE
                                                    </div>
                                                    <br />
                                                    @include('partials.questionnaires.openbutton')
                                                    <a href="/questionnaires/{{ $questionnaire->slug }}" name="view{{ $questionnaire->slug }}" class="button secondary tiny">
                                                        <i class="fas fa-question-circle"></i> Questions
                                                    </a>
                                                    <a href="/questionnaires/{{ $questionnaire->slug }}/edit" name="edit{{ $questionnaire->slug }}" class="button tiny">
                                                        <i class="fas fa-edit"></i> Edit
                                                    </a>
                                                @elseif($questionnaire->status == 1)
                                                    <div class="alert-box success large-text-center">
                                                        OPEN
                                                    </div>
                                                    <br />
                                                    @include('partials.questionnaires.closebutton')
                                                    <a href="/questionnaires/{{ $questionnaire->slug }}/responses" class="button info tiny">
                                                        <i class="fas fa-eye"></i> View Responses
                                                    </a>
                                                @else
                                                    <div class="alert-box alert large-text-center">
                                                        CLOSED
                                                    </div>
                                                    <br />
                                                    <a href="/questionnaires/{{ $questionnaire->slug }}/responses" class="button info tiny">
                                                        <i class="fas fa-eye"></i> View Responses
                                                    </a>
                                                @endif

                                                @include('partials.questionnaires.deletebutton')
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @else
                <div class="alert-box warning">
                    <i class="fas fa-exclamation-triangle"></i> You have no questionnaires
                </div>
            @endif
        @else
            <div class="alert-box alert">
                <i class="fas fa-times"></i> No questionnaires have been found
            </div>
        @endif
    </div>
@endsection