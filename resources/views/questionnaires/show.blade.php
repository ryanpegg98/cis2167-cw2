@extends('layouts.app')

@section('title', $questionnaire->title)

@section('breadcrumbs')
    <li role="menuitem"><a href="/">Home</a></li>
    <li role="menuitem"><a href="/questionnaires">Questionnaires</a></li>
    <li role="menuitem" class="current"><a href="/questionnaires/{{ $questionnaire->slug }}">{{ $questionnaire->title }}</a></li>
@endsection

@section('content')
    <?php
        $buttons = '';
    ?>
    <div class="row small-text-center">
        <div class="small-12 large-6 columns">
            @if($questionnaire->status == 0)
                <div class="alert-box large-text-center topButton">EDITABLE</div>
            @elseif($questionnaire->status == 1)
                <div class="alert-box success large-text-center topButton">OPEN</div>
            @else
                <div class="alert-box alert large-text-center topButton">CLOSED</div>
            @endif
        </div>
        <div class="small-12 large-6 columns large-text-right">
            @if($questionnaire->status == 0)
                @include('partials.questionnaires.openbutton')
                <a href="/questionnaires/{{ $questionnaire->slug }}/edit" name="edit{{ $questionnaire->slug }}" class="button tiny">
                    <i class="fas fa-edit"></i> Edit
                </a>
            @elseif($questionnaire->status == 1)
                @include('partials.questionnaires.closebutton')
                <a href="/questionnaires/{{ $questionnaire->slug }}/responses" class="button info tiny">
                    <i class="fas fa-eye"></i> View Responses
                </a>
            @else
                <a href="/questionnaires/{{ $questionnaire->slug }}/responses" class="button info tiny">
                    <i class="fas fa-eye"></i> View Responses
                </a>
            @endif
            @include('partials.questionnaires.deletebutton')
        </div>
    </div>
    <div class="row small-text-center">
        <div class="small-12 columns large-text-left">
            <h1>{{ $questionnaire->title }}</h1>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            @include('errors.messages')
        </div>
    </div>
    <br />
    <div class="row small-text-center">
        <div class="small-12 large-8 columns large-text-left">
            <h3>Questions:</h3>
        </div>
        <div class="small-12 large-4 columns large-text-right">
            <a href="/questionnaires/{{ $questionnaire->slug }}/questions/create" class="button success tiny">
                <i class="fas fa-plus"></i> Add Question
            </a>
        </div>
        <div class="small-12 columns large-text-left">
            <div class="dataBox">
                @if(isset($questions))
                    @if(count($questions) > 0)
                        <div class="row">
                            <div class="small-12 columns">
                                <ul class="accordion" data-accordion>
                                    @foreach($questions as $question)
                                        <?php $type = 'Text'; ?>
                                        @if($question->type == 2)
                                            <?php $type = 'Paragraph';?>
                                        @elseif($question->type == 3)
                                            <?php $type = 'Options';?>
                                        @elseif($question->type == 4)
                                            <?php $type = 'Scale';?>
                                        @endif
                                        <li class="accordion-navigation">
                                            <a href="#question{{ $question->slug }}">{{ $question->question }}</a>
                                            <div class="content small-text-center" id="question{{ $question->slug }}">
                                                <div class="row">
                                                    <div class="small-12 large-6 columns large-text-left">
                                                        <div class="row">
                                                            <div class="small-12 large-4 columns">
                                                                <label class="inline">Type:</label>
                                                            </div>
                                                            <div class="small-12 large-8 columns">
                                                                <input type="text" value="{{ $type }}" readonly/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="small-12 large-6 columns large-text-right">
                                                        @if($question->type == 3)
                                                            <a href="/questionnaires/{{ $questionnaire->slug }}/questions/{{ $question->slug }}" class="button secondary tiny">
                                                                <i class="fas fa-list-ul"></i> Options
                                                            </a>
                                                        @elseif($question->type == 4)
                                                            <a href="/questionnaires/{{ $questionnaire->slug }}/questions/{{ $question->slug }}/scales" class="button secondary tiny">
                                                                <i class="fas fa-balance-scale"></i> Change Scale
                                                            </a>
                                                        @endif
                                                        <a href="/questionnaires/{{ $questionnaire->slug }}/questions/{{ $question->slug }}/edit" class="button tiny">
                                                            <i class="fas fa-edit"></i> Edit
                                                        </a>
                                                        <a href="#" class="button alert tiny">
                                                            <i class="fas fa-trash-alt"></i> Delete
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @else
                        <div class="alert-box alert">
                            <i class="fas fa-exclamation-triangle"></i> You have not added any questions.
                        </div>
                    @endif
                @else
                    <div class="alert-box alert">
                        <i class="fas fa-times"></i> No questions have been added.
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection