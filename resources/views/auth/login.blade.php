@extends('layouts.app')

@section('title', 'Login')

@section('breadcrumbs')
    <li><a href="/">Home</a></li>
    <li><a href="/login">Login</a></li>
@endsection

@section('content')

    <div class="content">
        <div class="row">
            <h1>Login</h1>
            <div class="small-12 medium-6 large-4 columns">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}
                    <label for="email">E-Mail Address</label>
                    <input id="email" type="email" name="email" value="{{ old('email') }}">
                    <label for="password">Password</label>
                    <input id="password" type="password" name="password">
                    <label for="remember">
                        <input type="checkbox" name="remember"> Remember Me
                    </label>

                    <button type="submit" class="button tiny">
                        <i class="fas fa-sign-in-alt"></i> Login
                    </button>

                    <a class="tiny" href="{{ url('/password/reset') }}">Forgot Your Password?</a>

                </form>
            </div>
            <div class="small-12 medium-6 large-8 columns">
                @include('errors.errorlist')
            </div>
        </div>
    </div>

@endsection
