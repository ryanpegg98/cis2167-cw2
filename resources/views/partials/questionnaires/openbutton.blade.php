{!! Form::open(['action' => 'QuestionnairesController@open_questionnaire', 'style' => 'display: inline', 'onsubmit' => 'return confirm("Are you sure?")']) !!}
    {!! Form::hidden('questionnaire', $questionnaire->slug) !!}
    <button type="submit" class="button success tiny">
        <i class="fas fa-lock-open"></i> Open
    </button>
{!! Form::close() !!}