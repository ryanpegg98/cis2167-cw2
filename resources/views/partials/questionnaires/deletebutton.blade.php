{!! Form::open(['method' => 'DELETE', 'route' => ['questionnaires.destroy', $questionnaire->slug], 'style' => 'display: inline', 'id' => 'delete' . $questionnaire->slug, 'onsubmit' => 'return confirm("Are you sure?")']) !!}
<button type="submit" class="button alert tiny">
    <i class="fas fa-trash-alt"></i> Delete
</button>
{!! Form::close() !!}