{!! Form::open(['action' => 'QuestionnairesController@close_questionnaire', 'style' => 'display: inline', 'onsubmit' => 'return confirm("Are you sure?")']) !!}
{!! Form::hidden('questionnaire', $questionnaire->slug) !!}
<button type="submit" class="button warning tiny">
    <i class="fas fa-lock"></i> Close
</button>
{!! Form::close() !!}