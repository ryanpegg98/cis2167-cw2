<div class="row">
    <div class="small-12 large-6 columns">
        <div class="row">
            <div class="small-12 large-3 columns">
                {!! Form::label('title', 'Title:', ['class' => 'inline']) !!}
            </div>
            <div class="small-12 large-9 columns">
                {!! Form::text('title') !!}
            </div>
        </div>

        <div class="row">
            <div class="small-12 large-3 columns">
                {!! Form::label('slug', 'Slug:', ['class' => 'inline']) !!}
            </div>
            <div class="small-12 large-9 columns">
                {!! Form::text('slug') !!}
            </div>
        </div>

        <div class="row">
            <div class="small-12 columns">
                <div class="alert-box info">
                    <i class="fas fa-info-circle"></i> The slug cannot have any spaces.
                </div>
            </div>
        </div>
    </div>
    <div class="small-12 large-6 columns">
        <div class="row">
            <div class="small-12 large-3 columns">
                {!! Form::label('layout', 'Layout:', ['class' => 'inline']) !!}
            </div>
            <div class="small-12 large-9 columns">
                {!! Form::select('layout', [ 1 => 'Listed', 2 => 'One-by-One']) !!}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="small-12 large-6 columns">
        {!! Form::label('description', 'Description:') !!}
        {!! Form::textarea('description') !!}
    </div>
    <div class="small-12 large-6 columns">
        {!! Form::label('agreement', 'Ethical Agreement:') !!}
        {!! Form::textarea('agreement') !!}
    </div>
</div>