{!! Form::label('question', 'Question:') !!}
{!! Form::text('question') !!}

{!! Form::label('slug', 'Slug:') !!}
{!! Form::text('slug') !!}
<div class="row">
    <div class="small-12 columns">
        <div class="alert-box info">
            <i class="fas fa-info-circle"></i> The slug cannot have any spaces.
        </div>
    </div>
</div>

{!! Form::label('type', 'Question Type:') !!}
{!! Form::select('type', ['1' => 'Text', '2' => 'Paragraph', '3' => 'Options', '4' => 'Scale']) !!}