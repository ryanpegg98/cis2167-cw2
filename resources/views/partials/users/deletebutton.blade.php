{!! Form::open(['method' => 'DELETE', 'route' => ['admin.users.destroy', $user->id], 'style' => 'display: inline', 'id' => 'deleteuser' . $user->id, 'onsubmit' => 'return confirm("Are you sure?")']) !!}
<button type="submit" class="button alert tiny">
    <i class="fas fa-trash-alt"></i> Delete
</button>
{!! Form::close() !!}