<div class="row">
    <div class="small-12 large-4 columns">
        {!! Form::label('password', 'Password:', ['class' => 'inline']) !!}
    </div>
    <div class="small-12 large-8 columns">
        {!! Form::text('password') !!}
    </div>
</div>