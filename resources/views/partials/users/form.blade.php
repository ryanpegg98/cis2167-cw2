<div class="row">
    <div class="small-12 large-4 columns">
        {!! Form::label('name', 'Name:', ['class' => 'inline']) !!}
    </div>
    <div class="small-12 large-8 columns">
        {!! Form::text('name') !!}
    </div>
</div>

<div class="row">
    <div class="small-12 large-4 columns">
        {!! Form::label('email', 'Email Address:', ['class' => 'inline']) !!}
    </div>
    <div class="small-12 large-8 columns">
        {!! Form::text('email') !!}
    </div>
</div>

