@if($errors->any())
    @foreach($errors->all() as $error)
        <div class="alert-box alert tiny">
            <i class="fas fa-times"></i> {{ $error }}
        </div>
    @endforeach
@endif