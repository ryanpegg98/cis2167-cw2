@if(Session::has('success'))
    <div class="alert-box success">
        <i class="fas fa-check"></i> {{ Session::get('success') }}
    </div>
@elseif(Session::has('error'))
    <div class="alert-box alert">
        <i class="fas fa-times"></i> {{ Session::get('error') }}
    </div>
@endif