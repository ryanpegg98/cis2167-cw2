@extends('layouts.master')

@section('body')

    @include('includes.navbar')

    <div class="pageContent">
        @yield('content')
    </div>

@endsection