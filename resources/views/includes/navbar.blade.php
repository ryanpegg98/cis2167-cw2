
    <nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area">
            <li class="name">
                <h1><a href="#">Questionnaires</a></h1>
            </li>
            <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
        </ul>

        <section class="top-bar-section">


            @if(!Auth::guest())
                <ul class="left">
                    <li><a href="/">Home</a></li>
                    <li><a href="/questionnaires">My Questionnaires</a></li>
                    @can('see_adminnav')
                        <li class="has-dropdown">
                            <a href="/admin/users">Users</a>
                            <ul class="dropdown">
                                <li><a href="/admin/users">All Users</a></li>
                                <li><a href="/admin/questionnaires">All Questionnaires</a></li>
                            </ul>
                        </li>
                    @endcan
                </ul>
            @endif

            <!-- Right Nav Section -->
            <ul class="right">

                @if(Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                    <li class="has-dropdown">
                        <a href="#"><i class="fas fa-user"></i> {{ Auth::user()->name }}</a>
                        <ul class="dropdown">
                            <li><a href="/user/edit"><i class="fas fa-cog"></i> Settings</a></li>
                            <li><a href="{{ url('/logout') }}"><i class="fas fa-sign-out-alt"></i> Logout</a></li>
                        </ul>
                    </li>
                @endif

            </ul>
        </section>
    </nav>
    <nav class="breadcrumbs" role="menubar" aria-label="breadcrumbs">
        @yield('breadcrumbs')
    </nav>
