@extends('layouts.app')

@section('title', 'Reset Password - ' . $user->name)

@section('breadcrumbs')
    <li role="menuitem"><a href="/">Home</a></li>
    <li role="menuitem"><a href="/admin/users">Users</a></li>
    <li role="menuitem"><a href="/admin/users/{{ $user->id }}">{{ $user->name }}</a></li>
    <li role="menuitem" class="current"><a href="/admin/users/{{ $user->id }}/reset_password">Reset Password</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="small-12 columns">
            <h1>Edit - {{ $user->name }}</h1>
        </div>
    </div>
    <div class="row">
        <div class="small-12 large-6 columns">
        {!! Form::model($user, ['method' => 'PATCH', 'url' => '/admin/users/' . $user->id . '/password']) !!}

            {!! csrf_field() !!}

            <div class="row">
                <div class="small-12 large-4 columns">
                    {!! Form::label('new_password', 'New Password:', ['class' => 'inline']) !!}
                </div>
                <div class="small-12 large-8 columns">
                    {!! Form::text('new_password') !!}
                </div>
            </div>

            <div class="row">
                <div class="small-12 large-4 columns">
                    {!! Form::label('confirm_password', 'Confirm Password:', ['class' => 'inline']) !!}
                </div>
                <div class="small-12 large-8 columns">
                    {!! Form::text('confirm_password') !!}
                </div>
            </div>

            {!! Form::submit('Reset Password', ['class' => 'button success tiny right']) !!}
        {!! Form::close() !!}
        </div>
        <div class="small-12 large-6 columns">
            <div class="alert-box warning">
                <i class="fas fa-exclamation-triangle"></i> You are responsible to share the defined password with the {{ $user->name }}.
            </div>
            @include('errors.errorlist')
        </div>
    </div>
@endsection