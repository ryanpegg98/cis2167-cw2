@extends('layouts.app')

@section('title', 'Users')

@section('breadcrumbs')
    <li role="menuitem"><a href="/">Home</a></li>
    <li role="menuitem" class="current"><a href="/admin/users">Users</a></li>
@endsection

@section('content')

    <div class="row">
        <div class="small-12 columns">
            <div class="small-12 large-10 columns">
                <h1>Users</h1>
            </div>
            <div class="small-12 large-2 columns">
                <a href="/admin/users/create" class="button success tiny topButton"><i class="fas fa-plus"></i> Create User</a>
            </div>
        </div>
    </div>
    <div class="row">
        @include('errors.messages')
    </div>
    <div class="row dataBox">
        <div class="small-12 columns">

            @if(isset($users))
                <ul class="accordion" data-accordion>
                    @foreach($users as $user)
                        <li class="accordion-navigation">
                            <a href="#user{{ $user->id }}">{{ $user->name }}</a>
                            <div id="user{{ $user->id }}" class="content small-text-center">
                                <div class="row">
                                    <div class="small-12 large-6 columns large-text-left">
                                        <div class="row">
                                            <div class="small-12 large-4 columns">
                                                <label class="inline">Email Address:</label>
                                            </div>
                                            <div class="small-12 large-8 columns">
                                                <input type="text" value="{{ $user->email }}" readonly/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="small-12 large-4 columns">
                                                <label class="inline">Roles:</label>
                                            </div>
                                            <div class="small-12 large-8 columns">
                                                <div class="panel">
                                                    @foreach($user->roles as $role)
                                                        <span class="label">{{ $role->label }}</span>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="small-12 large-6 columns large-text-right">
                                        <a href="/admin/users/{{ $user->id }}" class="button success tiny">
                                            <i class="fas fa-user"></i> View
                                        </a>
                                        <a href="/admin/users/{{ $user->id }}/edit" name="edit{{ $user->id }}" class="button tiny">
                                            <i class="fas fa-edit"></i> Edit
                                        </a>
                                        <a href="/admin/users/{{ $user->id }}/reset_password" class="button warning tiny">
                                            <i class="fas fa-key"></i> Reset Password
                                        </a>
                                        @include('partials.users.deletebutton')
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            @else
                <div class="alert-box alert">
                    <i class="fas fa-times"></i> There are no users that can be found.
                </div>
            @endif
        </div>
    </div>

@endsection
