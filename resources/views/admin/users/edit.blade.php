@extends('layouts.app')

@section('title', 'Edit - ' . $user->name)

@section('breadcrumbs')
    <li role="menuitem"><a href="/">Home</a></li>
    <li role="menuitem"><a href="/admin/users">Users</a></li>
    <li role="menuitem"><a href="/admin/users/{{ $user->id }}">{{ $user->name }}</a></li>
    <li role="menuitem" class="current"><a href="/admin/users/{{ $user->id }}/edit">Edit</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="small-12 columns">
            <h1>Edit - {{ $user->name }}</h1>
        </div>
    </div>
    <div class="row">
        <div class="small-12 large-6 columns">
            {!! Form::model($user, ['method' => 'PATCH', 'url' => '/admin/users/' . $user->id]) !!}

                {!! csrf_field() !!}
                @include('partials.users.form')

                <!-- Only allow certain users assign roles -->

                @can('assign_roles')
                    <div class="row">
                        <div class="small-12 large-4 columns">
                            {!! Form::label('roles', 'Roles:', ['class' => 'inline']) !!}
                        </div>
                        <div class="small-12 large-8 columns">
                            <div class="panel">
                                @foreach($roles as $role)
                                    <div class="row">
                                        {!! Form::label($role->id, $role->name, ['class' => 'left']) !!}
                                        {!! Form::checkbox('role[]', $role->id, $user->roles->contains($role->id), ['id' => $role->id, 'class' => 'left']) !!}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endcan

                {!! Form::submit('Edit', ['class' => 'button success tiny right']) !!}

            {!! Form::close() !!}
        </div>
        <div class="small-12 large-6 columns">
            @include('errors.errorlist')
        </div>
    </div>
@endsection