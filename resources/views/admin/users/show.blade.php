@extends('layouts.app')

@section('title', $user->name)

@section('breadcrumbs')
    <li role="menuitem"><a href="/">Home</a></li>
    <li role="menuitem"><a href="/admin/users">Users</a></li>
    <li role="menuitem" class="current"><a href="/admin/users/{{ $user->id }}">{{ $user->name }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="small-12 large-6 columns">
            <h1>{{ $user->name }}</h1>
        </div>
        <div class="small-12 large-6 columns large-text-right">
            <a href="/admin/users/{{ $user->id }}/edit" class="button tiny topButton">
                <i class="fas fa-edit"></i> Edit
            </a>
            <a href="/admin/users/{{ $user->id }}/reset_password" class="button warning tiny topButton">
                <i class="fas fa-key"></i> Reset Password
            </a>
            @include('partials.users.deletebutton')
        </div>
    </div>
    <div class="row" data-equalizer>
        <div class="small-12 large-6 columns panel" data-equalizer-watch>
            <h4>User's Details</h4>
            <br />
            <div class="row">
                <div class="small-12 large-4 columns">
                    <label class="inline">Email Address:</label>
                </div>
                <div class="small-12 large-8 columns">
                    <input type="text" value="{{ $user->email }}" readonly/>
                </div>
            </div>
            <div class="row">
                <div class="small-12 large-4 columns">
                    <label class="inline">Roles:</label>
                </div>
                <div class="small-12 large-8 columns">
                    <div>
                        @foreach($user->roles as $role)
                            <span class="label">{{ $role->label }}</span>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="small-12 large-6 columns panel" data-equalizer-watch>
            <h4>User's Questionnaires</h4>

            @if(isset($questionnaires))
                @if(count($questionnaires) > 0)
                    @foreach($questionnaires as $questionnaire)
                        <div class="panel">
                            <div class="row">
                                <div class="small-12 large-6 columns">
                                    {{ $questionnaire->title }}
                                </div>
                                <div class="small-12 large-6 columns">
                                    Options
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="alert-box alert">
                        <i class="fas fa-exclamation-triangle"></i> No Questionnaires Created
                    </div>
                @endif

            @else
                <div class="alert-box alert">
                    <i class="fas fa-times"></i> No Questionnaires Created
                </div>
            @endif
        </div>
    </div>
@endsection