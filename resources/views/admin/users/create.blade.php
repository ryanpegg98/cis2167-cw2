@extends('layouts.app')

@section('title', 'Create User')

@section('breadcrumbs')
    <li role="menuitem"><a href="/">Home</a></li>
    <li role="menuitem"><a href="/admin/users">Users</a></li>
    <li role="menuitem" class="current"><a href="/admin/users/create">Create</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="small-12 columns">
            <h1>Create User</h1>
        </div>
    </div>

    <div class="row">
        <div class="small-12 large-6 columns">
            {!! Form::open(['url' => '/admin/users', 'id' => 'createuser']) !!}

                {!! csrf_field() !!}

                @include('partials.users.form')

                @include('partials.users.password')

                {!! Form::submit('Create User', ['class' => 'button success tiny right']) !!}

            {!! Form::close() !!}
        </div>
        <div class="small-12 large-6 columns">
            <div class="alert-box warning">
                <i class="fas fa-exclamation-triangle"></i> You are responsible to share the defined password with the new user.
            </div>
            @include('errors.errorlist')
        </div>
    </div>
@endsection